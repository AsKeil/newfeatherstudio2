﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Classe qui gère le menu pause.
/// </summary>
public class MenuPause : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject canvas;

    public GameObject canvasPause;

    /// <summary>
    /// Met a jour le jeu si il est en pause ou non.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                canvas.SetActive(true);
                canvasPause.SetActive(false);
                Continue();
            }
            else
            {
                canvasPause.SetActive(true);
                canvas.SetActive(false);
                Pause();
            }
        }
    }

    /// <summary>
    /// Fait continuer le jeu.
    /// </summary>
    public void Continue()
    {
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    /// <summary>
    /// Fait pauser le jeu.
    /// </summary>
    public void Pause()
    {
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    /// <summary>
    /// Charge le menu principal du jeu.
    /// </summary>
    public void LoadMenuPrincipal()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MenuDuJeu");
    }

    /// <summary>
    /// Charge le village du jeu.
    /// </summary>
    public void LoadVillage()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("SceneVillage");
    }

    /// <summary>
    /// Sauvegarde la partie.
    /// </summary>
    public void SauvegarderPartie()
    {
        ApplicationData.SauvegarderPartie();
    }

    /// <summary>
    /// Quitte le jeu.
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }


}
