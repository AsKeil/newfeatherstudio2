﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "BatimentsConstruits" dans la base de données "Partie"
    /// </summary>
    public class BatimentsConstruitsBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _batimentsEid = "BatimentsEid";
        private const string _nombreDeVillageoisAssignes = "nombreDeVillageoisAssignes";
        private const string _pointsDeVieActuels = "pointsDeVieActuels";
        private const string _positionX = "positionX";
        private const string _positionY = "positionY";
        private const string _clePrimaire = "id";
        private BatimentsEBd _batimentsEBd;

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdPartie"> Le nom de la BD de partie.</param>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD encyclopédie.</param>
        public BatimentsConstruitsBd(string nomBdPartie, string nomBdEncyclopedie) : base(nomBdPartie, "BatimentsConstruits")
        {
            _batimentsEBd = new BatimentsEBd(nomBdEncyclopedie);
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _batimentsEid + " INTEGER NOT NULL, "
                + _nombreDeVillageoisAssignes + " INTEGER NOT NULL, "
                + _pointsDeVieActuels + " INTEGER NOT NULL, "
                + _positionX + " INTEGER NOT NULL, "
                + _positionY + " INTEGER NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="batiment"> L'entité à ajouter dans la BD.</param>
        public void AddData(BatimentEntity batiment)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _batimentsEid + ", "
                + _nombreDeVillageoisAssignes + ", "
                + _pointsDeVieActuels + ", "
                + _positionX + ", "
                + _positionY
                + " ) "

                + "VALUES ( @nom"
                + ", @nombreDeVillageoisAssignes"
                + ", @pointsDeVieActuels"
                + ", @positionX"
                + ", @positionY"
                + " )";
            AddParameterWithValue(dbcmd, "@nom", _batimentsEBd.GetIdFromNom(batiment.nom));
            AddParameterWithValue(dbcmd, "@nombreDeVillageoisAssignes", batiment.nombreDeVillageoisAssignes);
            AddParameterWithValue(dbcmd, "@pointsDeVieActuels", batiment.pointsDeVieActuels);
            AddParameterWithValue(dbcmd, "@positionX", batiment.positionX);
            AddParameterWithValue(dbcmd, "@positionY", batiment.positionY);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<BatimentEntity> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<BatimentEntity> liste = BatimentsConstruitsEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<BatimentEntity> BatimentsConstruitsEntityParser(IDataReader reader)
        {
            List<BatimentEntity> liste = new List<BatimentEntity>();
            while (reader.Read())
            {
                BatimentEntity batimentC = new BatimentEntity(
                    _batimentsEBd.GetEntityFromId(reader[1].ToString()),
                    int.Parse(reader[2].ToString()),
                    int.Parse(reader[3].ToString()),
                    int.Parse(reader[4].ToString()),
                    int.Parse(reader[5].ToString()));
                liste.Add(batimentC);
            }
            return liste;
        }

        /// <summary>
        /// Ferme toutes les connexions.
        /// </summary>
        public new void Close()
        {
            _batimentsEBd.Close();
            bdConnexion.Close();
            bdConnexion.Dispose();
        }
    }
}
