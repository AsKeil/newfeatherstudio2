﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestTuile
    {
        /// <summary>
        /// Verifie que la tuille possede le bon materiel selon son tag.
        /// L'ajout de " (Instance)" a la derniere ligne de code est nécéssaire car
        /// la tuile est un clone d'un préfab et donc le renderer est instance et ajoute 
        /// " (Instance)" a la fin du nom de son materiel.
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator TestTuileMateriel()
        {
            var tuile = GameObject.Instantiate(Resources.Load("Prefabs/Tuile") as GameObject);
            tuile.tag = "Tuile,Foret";
            yield return null;
           
            tuile.gameObject.GetComponent<Tuile>().RetourMaterielPrecedant();          
            Assert.IsTrue(tuile.gameObject.GetComponent<Renderer>().material.name.
                Equals(Resources.Load<Material>("Materials/Foret").name +" (Instance)"));
        }
    }
}
