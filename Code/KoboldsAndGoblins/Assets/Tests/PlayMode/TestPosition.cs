﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    /// <summary>
    /// Fonction de test qui verifie que le combatant allié trouve 
    /// ses positions adjascentes correctement
    /// </summary>
    public class TestPosition
    {
        [UnityTest]
        public IEnumerator TestPositionGauche()
        {
            var allie = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Allie") as GameObject);
            allie.transform.position = new Vector3(0, 1.4f, 0);
            allie.GetComponent<ActionCombatants>().PositionAdjacente();
            
            yield return null;
            
            Assert.AreEqual(new Vector3(-1, 1.4f, 0), allie.GetComponent<ActionCombatants>().positioAdjascenteGauche);
            Object.DestroyImmediate(allie);
        }
        [UnityTest]
        public IEnumerator TestPositionDroite()
        {
            var allie = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Allie") as GameObject);
            allie.transform.position = new Vector3(0, 1.4f, 0);
            allie.GetComponent<ActionCombatants>().PositionAdjacente();

            yield return null;
            
            Assert.AreEqual(new Vector3(1, 1.4f, 0), allie.GetComponent<ActionCombatants>().positioAdjascenteDroite);
            Object.DestroyImmediate(allie);
        }
        [UnityTest]
        public IEnumerator TestPositionAvant()
        {
            var allie = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Allie") as GameObject);
            allie.transform.position = new Vector3(0, 1.4f, 0);
            allie.GetComponent<ActionCombatants>().PositionAdjacente();         
            
            yield return null;
            
            Assert.AreEqual(new Vector3(0, 1.4f, 1), allie.GetComponent<ActionCombatants>().positioAdjascenteAvant);
            Object.DestroyImmediate(allie);
        }
        [UnityTest]
        public IEnumerator TestPositionArriere()
        {
            var allie = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Allie") as GameObject);
            allie.transform.position = new Vector3(0, 1.4f, 0);
            allie.GetComponent<ActionCombatants>().PositionAdjacente();
            
            yield return null;
            
            Assert.AreEqual(new Vector3(0, 1.4f, -1), allie.GetComponent<ActionCombatants>().positioAdjascenteArriere);
            Object.DestroyImmediate(allie);
        }
    }
}
