﻿

namespace Models
{
    /// <summary>
    /// DEF-13-5 Sauvegarde
    /// REQ-3-1 Créer une nouvelle sauvegarde
    /// Représente une sauvegarde de la partie pour permettre le chargement.
    /// </summary>
    public class SauvegardeEntity
    {
        public string _nomVillage;
        public string _dateHeure;

        /// <summary>
        /// Constructeur pour les nouvelles sauvegardes.
        /// </summary>
        /// <param name="nomVillage">Le nom du village pour la sauvegarde</param>
        public SauvegardeEntity(string nomVillage)
        {
            _nomVillage = nomVillage;
        }

        /// <summary>
        /// Constructeur pour une sauvegarde déjà existante.
        /// </summary>
        /// <param name="nomVillage">Le nom du village pour la sauvegarde</param>
        /// <param name="dateHeure">La date et l'heure de la sauvegarde</param>
        public SauvegardeEntity(string nomVillage, string dateHeure)
        {
            _nomVillage = nomVillage;
            _dateHeure = dateHeure;
        }
    }
}
