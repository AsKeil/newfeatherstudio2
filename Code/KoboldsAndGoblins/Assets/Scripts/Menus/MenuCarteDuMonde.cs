﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Classe qui gère le chnagement de scene de la phase de la carte du monde.
/// </summary>
public class MenuCarteDuMonde : MonoBehaviour
{
    /// <summary>
    /// Charge le village du jeu.
    /// </summary>
    public void LoadVillage()
    {
        SceneManager.LoadScene("SceneVillage");
    }

    /// <summary>
    /// Permet de changer la scene vers celle de combat du campement 
    /// (une seul map sera utiliser pour la preuve de concept).
    /// </summary>
    public void AttaquerCampement()
    {
        SceneManager.LoadScene("GrilleCombat");
    }

    /// <summary>
    /// Permet de changer la scene vers celle de combat du Village 
    /// (une seul map sera utiliser pour la preuve de concept).
    /// </summary>
    public void AttaquerVillage()
    {
        SceneManager.LoadScene("GrilleCombat");
    }

}
