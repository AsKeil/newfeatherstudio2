Quel est le problème rencontré?

(Ce qui s'est produit.)

Quel est le résultat attendu?

(Ce qui devrait se produire.)

Détails Supplémentaires

(Détails si nécessaire.)

Capture d'écran du problème

(Capture d'écran si disponible.)

/label ~Bug
