﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using UnityEngine.SceneManagement;
using TMPro;
using DataBank;
using UnityEngine.UI;

/// <summary>
/// Classe qui gère l'interface de création d'une nouvelle partie.
/// </summary>
public class Nouvellepartie : MonoBehaviour
{
    public TMP_Dropdown RaceDropdown;

    public TMP_InputField VillageinputField;

    public Button buttonCreer;

    /// <summary>
    /// Place les races du jeu dans le dropdown.
    /// </summary>
    void Start()
    {
        buttonCreer.interactable = false;

        RaceDropdown.ClearOptions();

        List<string> optionslist = new List<string>();

        foreach (RaceEntity race in ApplicationData.ListeRacesE)
        {

            optionslist.Add(race.nomRace);

        }

        RaceDropdown.AddOptions(optionslist);

    }

    void Update()
    {
        if (VillageinputField.text != "")
        {
            buttonCreer.interactable = true;
        }
        else
        {
            buttonCreer.interactable = false;
        }

    }

    /// <summary>
    /// Trouve le nom du village dans le input du menu.
    /// </summary>
    /// <returns> le nom du village </returns>
    public string TrouverNomVillage()
    {
        string nomVillage = VillageinputField.text;

        return nomVillage;

    }

    /// <summary>
    /// Trouve la race dans le dropdown du menu.
    /// </summary>
    /// <returns> la race </returns>
    public RaceEntity TrouverRace()
    {
        string nomRaceSelectionner = RaceDropdown.options[RaceDropdown.value].text;

        RaceEntity race = ApplicationData.ListeRacesE.Find(x => x.nomRace.Equals(nomRaceSelectionner));

        return race;
    }

    /// <summary>
    /// Crée une partie de jeu.
    /// </summary>
    public void creerPartie()
    {
        ApplicationData.NouvellePartie(TrouverRace(), TrouverNomVillage());
        ApplicationData.SauvegarderPartie();

        SceneManager.LoadScene("SceneVillage");
     
    }

    

    



}
