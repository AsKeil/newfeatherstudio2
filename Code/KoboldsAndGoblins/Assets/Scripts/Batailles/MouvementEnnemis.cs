﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class initialisant le mouvement des ennemis.
/// </summary>
public class MouvementEnnemis : MouvementCombatants
{
    //Combattant du joueur


    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<ActionEnnemis>().tourCombatantActif)
        {
            if (GetTuilecourante() == null)
            {
                SetTuileCourrante();
            }
            if (!enMouvement)
            {                
                TrouverChemin(TrouveCibleProche());
                if (tuileSelectionable.Count == 0)
                    TrouverTuileSelectionable();
                veritableTuileCible.cible = true;
            }
            else if (!finMouvement)
            {
                DeplacementCombattant();                
            }
            if (finMouvement)
            {
                SetTuileCourrante();
            }
        }                
    }
   
    /// <summary>
    ///  Fonction permettant aux ennemis de trouver le combattant du joueur le plus proche.
    ///  Enregistre ensuite la cible pour permetre le calcule du chemin.
    /// </summary>
    private GameObject TrouveCibleProche()
    {
        GameObject[] cibles = GameObject.FindGameObjectsWithTag("Allie");

        GameObject proche = null;

        float distance = Mathf.Infinity;

        foreach (GameObject obj in cibles)
        {
            float d = Vector3.Distance(transform.position, obj.transform.position);
            if (d < distance)
            {
                distance = d;
                proche = obj;
            }
        }
        return proche;
    }   
}
