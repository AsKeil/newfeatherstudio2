﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Models;

namespace Tests
{
    public class TestsGererRessources
    {
        public GameObject TestObjet;

        /// <summary>
        /// Permet de créer un environnement semi-indépendant du jeu en soit
        /// pour pouvoir tester le script GererRessources.
        /// </summary>
        void CreerEnvironnementTest()
        {
            RaceEntity race = new RaceEntity("Bob");
            ApplicationData.NouvellePartie(race, "ASD");
            ApplicationData.village.nourriture = 100;
            ApplicationData.village.bois = 100;
            ApplicationData.village.pierre = 100;
            ApplicationData.village.or = 100;
            TestObjet = new GameObject();
            TestObjet.AddComponent<GererRessources>();
        }

        /// <summary>
        /// Test pour savoir si la vérification du coût de l'ensemble des ressources 
        /// par rapport à l'ensemble des ressources disponible se fait sans problème.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierCoutRessources()
        {
            CreerEnvironnementTest();
            Assert.AreEqual(TestObjet.GetComponent<GererRessources>().VerifierCoutRessources(10, 10, 10, 10),true);
            yield return null;
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour savoir si la vérification du coût de nourriture 
        /// par rapport à la quantitré de nourriture disponible se fait sans problème.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierCoutNourriture()
        {
            CreerEnvironnementTest();
            Assert.AreEqual(TestObjet.GetComponent<GererRessources>().VerifierCoutNourriture(10), true);
            yield return null;
            ApplicationData.PartieNull();
        }


        /// <summary>
        /// Test pour savoir si la vérification du coût de bois 
        /// par rapport à la quantité de bois disponible se fait sans problème.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierCoutBois()
        {
            CreerEnvironnementTest();
            Assert.AreEqual(TestObjet.GetComponent<GererRessources>().VerifierCoutBois(10), true);
            yield return null;
            ApplicationData.PartieNull();
        }


        /// <summary>
        /// Test pour savoir si la vérification du coût de pierre 
        /// par rapport à la quantité de pierre disponible se fait sans problème.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierCoutPierre()
        {
            CreerEnvironnementTest();
            Assert.AreEqual(TestObjet.GetComponent<GererRessources>().VerifierCoutPierre(10), true);
            yield return null;
            ApplicationData.PartieNull();
        }


        /// <summary>
        /// Test pour savoir si la vérification du coût d'or 
        /// par rapport à la quantité d'or disponible se fait sans problème.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierCoutOr()
        {
            CreerEnvironnementTest();
            Assert.AreEqual(TestObjet.GetComponent<GererRessources>().VerifierCoutOr(10), true);
            yield return null;
            ApplicationData.PartieNull();
        }


        /// <summary>
        /// Test pour diminuer les ressources avec un coût inférieur aux ressources disponibles. 
        /// </summary>
        [UnityTest]
        public IEnumerator TestEnleverRessourceInferieur()
        {
            CreerEnvironnementTest();
            TestObjet.GetComponent<GererRessources>().EnleverRessource(10, 20, 30, 40);
            Assert.AreEqual(ApplicationData.village.nourriture, 90);
            Assert.AreEqual(ApplicationData.village.bois, 80);
            Assert.AreEqual(ApplicationData.village.pierre, 70);
            Assert.AreEqual(ApplicationData.village.or, 60);
            yield return null;
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour diminuer les ressources avec un coût équivalent aux ressources disponibles. 
        /// </summary>
        [UnityTest]
        public IEnumerator TestEnleverRessourceEquivalent()
        {
            CreerEnvironnementTest();
            TestObjet.GetComponent<GererRessources>().EnleverRessource(100, 100, 100, 100);
            Assert.AreEqual(ApplicationData.village.nourriture, 0);
            Assert.AreEqual(ApplicationData.village.bois, 0);
            Assert.AreEqual(ApplicationData.village.pierre, 0);
            Assert.AreEqual(ApplicationData.village.or, 0);
            yield return null;
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour diminuer les ressources avec un seul coût supérieur aux ressources disponibles
        /// et les autres avec un coup équivalent ou inférieur. 
        /// </summary>
        [UnityTest]
        public IEnumerator TestEnleverRessourceSuperieurSeul()
        {
            CreerEnvironnementTest();
            TestObjet.GetComponent<GererRessources>().EnleverRessource(110, 20, 30, 40);
            Assert.AreEqual(ApplicationData.village.nourriture, 100);
            Assert.AreEqual(ApplicationData.village.bois, 100);
            Assert.AreEqual(ApplicationData.village.pierre, 100);
            Assert.AreEqual(ApplicationData.village.or, 100);
            yield return null;
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour diminuer les ressources avec un coût supérieur aux ressources disponibles. 
        /// </summary>
        [UnityTest]
        public IEnumerator TestEnleverRessourceSuperieurTous()
        {
            CreerEnvironnementTest();
            TestObjet.GetComponent<GererRessources>().EnleverRessource(110, 110, 110, 110);
            Assert.AreEqual(ApplicationData.village.nourriture, 100);
            Assert.AreEqual(ApplicationData.village.bois, 100);
            Assert.AreEqual(ApplicationData.village.pierre, 100);
            Assert.AreEqual(ApplicationData.village.or, 100);
            yield return null;
            ApplicationData.PartieNull();
        }
    }
}
