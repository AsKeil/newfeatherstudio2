﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "Equipements" dans la base de données "Partie".
    /// </summary>
    public class EquipementsBd : SqliteHelper
    {
        private const string _champEquipementsEid = "EquipementsEid";
        private const string _champQuantite = "quantite";
        private const string _clePrimaire = "EquipementsEid";
        private EquipementsEBd _equipementsEBd;

        public string NomTable { get { return nom_table; } }
        public string ChampId { get { return _champEquipementsEid; } }

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdPartie"> Le nom de la BD de partie.</param>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD encyclopédie.</param>
        public EquipementsBd(string nomBdPartie, string nomBdEncyclopedie) : base(nomBdPartie, "Equipements")
        {
            this._equipementsEBd = new EquipementsEBd(nomBdEncyclopedie);
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champEquipementsEid + " INTEGER NOT NULL, "
                + _champQuantite + " INTEGER NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="equipement"> L'entité à ajouter dans la BD.</param>
        /// <param name="quantite"> La quantité à ajouter dans la BD.</param>
        public void AddData(EquipementsEntity equipement, int quantite)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champEquipementsEid + ", "
                + _champQuantite
                + " ) "

                + "VALUES ( @equipement"
                + ", @quantite"
                + " )";
            AddParameterWithValue(dbcmd, "@nomVillage", _equipementsEBd.GetIdFromEntity(equipement));
            AddParameterWithValue(dbcmd, "@nomVillage", quantite);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<(EquipementsEntity,int)> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<(EquipementsEntity, int)> liste = EquipementsEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<(EquipementsEntity, int)> EquipementsEntityParser(IDataReader reader)
        {
            List<(EquipementsEntity, int)> liste = new List<(EquipementsEntity, int)>();
            while (reader.Read())
            {
                EquipementsEntity equipement = _equipementsEBd.GetEntityFromId(reader[0].ToString());
                int quantite = int.Parse(reader[1].ToString());

                (EquipementsEntity, int) entree = (equipement, quantite);
                liste.Add(entree);
            }
            return liste;
        }

        /// <summary>
        /// Ferme toutes les connexions.
        /// </summary>
        public new void Close()
        {
            _equipementsEBd.Close();
            bdConnexion.Close();
            bdConnexion.Dispose();
        }
    }
}
