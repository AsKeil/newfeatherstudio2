﻿using System.Collections.Generic;
using System.IO;
using DataBank;
using Models;
using UnityEngine;

/// <summary>
/// Classe qui contient la progression de la partie et les accès aux bases de données.
/// </summary>
public static class ApplicationData
{
    public static string NomBdEncyclopedie { get; set; } = "bdEncyclopedie.db3";
    public static string NomBdSauvegarde { get; set; } = "bdSauvegarde.db3";
    public static string NomPartie { get; set; } = "bdPartie";

    public static VillageEntity village;
    public static List<CombattantsEntity> listeCombattants;
    public static List<BatimentEntity> listeBatimentsConstruits;
    public static List<(EquipementsEntity, int)> listeEquipementsQuantites;
    public static List<EvenementEntity> listeEvenements;

    public static List<SauvegardeEntity> ListeSauvegardes { get { return ListerSauvegardes(); } }
    public static List<RaceEntity> ListeRacesE { get { return ListerRacesE(); } }
    public static List<BatimentEntity> ListeBatimentsE { get { return ListerBatimentsE(); } }
    public static List<EquipementsEntity> ListeEquipementsE(string typeEquipement) { return ListerEquipementsE(typeEquipement); }

    public static System.Random random = new System.Random();

    /// <summary>
    /// Crée une nouvelle partie avec les valeurs de départs.
    /// REQ-3-7 Créer une partie.
    /// </summary>
    /// <param name="race">Le race du village</param>
    /// <param name="nomVillage">Le nom du village</param>
    public static void NouvellePartie(RaceEntity race, string nomVillage)
    {
        PartieNull();
        village = new VillageEntity(race, nomVillage);
        listeCombattants = new List<CombattantsEntity>();
        for (int i = 0; i != 10; i++)
        {
            CombattantsEntity combattant = new CombattantsEntity(village.race, null, null);
            listeCombattants.Add(combattant);
        }
        listeBatimentsConstruits = new List<BatimentEntity>();
        listeEquipementsQuantites = new List<(EquipementsEntity, int)>();
        listeEvenements = new List<EvenementEntity>();
    }

    /// <summary>
    /// Les données du jeu lors du chargement d'une partie.
    /// REQ-3-2 Charger une sauvegarde déjà existante.
    /// </summary>
    /// <param name="sauvegarde">La sauvegarde à charger.</param>
    public static void ChargerPartie(SauvegardeEntity sauvegarde)
    {
        PartieNull();
        
        // Va chercher le ID de la sauvegarde
        SauvegardesBd sauvegardesBd = new SauvegardesBd(NomBdSauvegarde);
        string nomPartie = NomPartie + sauvegardesBd.GetIdFromEntity(sauvegarde) + ".db3";
        sauvegardesBd.Close();
        
        // Charge le village
        VillagesBd villagesBd = new VillagesBd(nomPartie, NomBdEncyclopedie);
        village = villagesBd.GetListeEntity()[0];
        villagesBd.Close();

        // Charge les événements
        EvenementsBd evenementsBd = new EvenementsBd(nomPartie, NomBdEncyclopedie);
        listeEvenements = evenementsBd.GetListeEntity();
        evenementsBd.Close();
        
        // Charge les combattants
        CombatantsBd combatantsBd = new CombatantsBd(nomPartie, NomBdEncyclopedie);
        listeCombattants = combatantsBd.GetListeEntity();
        combatantsBd.Close();
        
        // Charge les bâtiments construits
        BatimentsConstruitsBd batimentsConstruitsBd = new BatimentsConstruitsBd(nomPartie, NomBdEncyclopedie);
        listeBatimentsConstruits = batimentsConstruitsBd.GetListeEntity();
        batimentsConstruitsBd.Close();
        
        // Charge les équipements
        EquipementsBd equipementsBd = new EquipementsBd(nomPartie, NomBdEncyclopedie);
        listeEquipementsQuantites = equipementsBd.GetListeEntity();
        equipementsBd.Close();
    }

    /// <summary>
    /// Sauvegarde les données de la partie.
    /// REQ-3-1 Créer une nouvelle sauvegarde .
    /// </summary>
    public static void SauvegarderPartie()
    {
        // Attend une seconde avant de sauvegarder pour s'assurer que le DateTime de la sauvegarde est différent du précédent.
        // Permet d'éviter certains bugs.
        System.Threading.Thread.Sleep(1000);

        // Crée la sauvegarde
        SauvegardesBd sauvegardesBd = new SauvegardesBd(NomBdSauvegarde);
        sauvegardesBd.AddData(new SauvegardeEntity(village.nom));
        string nomSauvegarde = NomPartie + sauvegardesBd.GetLastId() + ".db3";
        sauvegardesBd.Close();

        // Sauvegarde du village
        VillagesBd villageBd = new VillagesBd(nomSauvegarde, NomBdEncyclopedie);
        villageBd.AddData(village);
        villageBd.Close();

        // Sauvegarde des événements
        EvenementsBd evenementsBd = new EvenementsBd(nomSauvegarde, NomBdEncyclopedie);
        foreach (EvenementEntity evenement in listeEvenements)
        {
            evenementsBd.AddData(evenement);
        }
        evenementsBd.Close();

        // Sauvegarde des combattants
        CombatantsBd combattantsBd = new CombatantsBd(nomSauvegarde, NomBdEncyclopedie);
        foreach (CombattantsEntity combattant in listeCombattants)
        {
            combattantsBd.AddData(combattant);
        }
        combattantsBd.Close();

        // Sauvegarde des bâtiments construits
        BatimentsConstruitsBd batimentsConstruitsBd = new BatimentsConstruitsBd(nomSauvegarde, NomBdEncyclopedie);
        foreach (BatimentEntity batimentConstruit in listeBatimentsConstruits)
        {
            batimentsConstruitsBd.AddData(batimentConstruit);
        }
        batimentsConstruitsBd.Close();

        // Sauvegarde des equipements
        EquipementsBd equipementsBd = new EquipementsBd(nomSauvegarde, NomBdEncyclopedie);
        foreach ((EquipementsEntity, int) equipementQuantite in listeEquipementsQuantites)
        {
            equipementsBd.AddData(equipementQuantite.Item1, equipementQuantite.Item2);
        }
        equipementsBd.Close();
    }

    /// <summary>
    /// Supprime une sauvegarde.
    /// REQ-3-6 Supprimer une sauvegarde.
    /// </summary>
    /// <param name="sauvegarde">La sauvegarde à supprimer.</param>
    public static void SupprimerSauvegarde(SauvegardeEntity sauvegarde)
    {
        SauvegardesBd sauvegardesBd = new SauvegardesBd(NomBdSauvegarde);

        // Fait un appel pour se débarasser des éléments pending dans le "Garbage Collector" avant de faire la supression.
        // Permet d'éviter certains bugs.
        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();

        // Supprime la BD contenant les informations de la partie
        string id = sauvegardesBd.GetIdFromEntity(sauvegarde);
        string nomSauvegarde = NomPartie + id + ".db3";
        string fichierASupprimer = Application.persistentDataPath + "/" + nomSauvegarde;
        File.Delete(fichierASupprimer);

        // Suprimme l'entrée de la BD de sauvegarde
        sauvegardesBd.DeleteFromEntity(sauvegarde);
        sauvegardesBd.Close();
    }

    /// <summary>
    /// Permet de s'assurer que les données en lien avec la partie sont null.
    /// À appeler avant le début d'une nouvelle partie ou avant le chargement d'une partie.
    /// Sert de précaution. Il ne devrait pas y avoir de conséquences à ne pas utiliser cette méthode.
    /// Utile pour certains tests.
    /// </summary>
    public static void PartieNull()
    {
        village = null;
        listeCombattants = null;
        listeBatimentsConstruits = null;
        listeEquipementsQuantites = null;
        listeEvenements = null;
    }

    /// <summary>
    /// Retourne la liste de sauvegardes de la BD de sauvegardes.
    /// </summary>
    /// <returns>La liste des sauvegardes.</returns>
    private static List<SauvegardeEntity> ListerSauvegardes()
    {
        SauvegardesBd sauvegardesBd = new SauvegardesBd(NomBdSauvegarde);
        List<SauvegardeEntity> liste = sauvegardesBd.GetListeEntity();
        sauvegardesBd.Close();
        return liste;
    }

    /// <summary>
    /// Retourne la liste de races de l'encyclopédie.
    /// </summary>
    /// <returns>La liste des races de l'encyclopédie.</returns>
    private static List<RaceEntity> ListerRacesE()
    {
        RacesEBd racesEBd = new RacesEBd(NomBdEncyclopedie);
        List<RaceEntity> liste = racesEBd.GetListeEntity();
        racesEBd.Close();
        return liste;
    }

    /// <summary>
    /// Retourne la liste de bâtiments de l'encyclopédie.
    /// </summary>
    /// <returns>La liste des bâtiments de l'encyclopédie.</returns>
    private static List<BatimentEntity> ListerBatimentsE()
    {
        BatimentsEBd batimentsEBd = new BatimentsEBd(NomBdEncyclopedie);
        List<BatimentEntity> liste = batimentsEBd.GetListeEntity();
        batimentsEBd.Close();
        return liste;
    }

    /// <summary>
    /// Retourne la liste d'équipements de l'encyclopédie.
    /// Un string vide retourne tout les équipments.
    /// Un string non vide retourne tout les équipements de ce type.
    /// </summary>
    /// <param name="typeEquipement">Un type d'équipements: "", "arme", "armure".</param>
    /// <returns>Une liste d'équipements</returns>
    private static List<EquipementsEntity> ListerEquipementsE(string typeEquipement)
    {
        EquipementsEBd equipementsEBd = new EquipementsEBd(NomBdEncyclopedie);
        if (typeEquipement == "")
            return equipementsEBd.GetListeEntity();
        List<EquipementsEntity> liste = equipementsEBd.GetListeEntity(typeEquipement);
        equipementsEBd.Close();
        return liste;
    }
}
