﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Classe qui gère le retour au village.
/// </summary>
public class BoutonRetourVillage : MonoBehaviour
{
    /// <summary>
    /// fonction permettant de retourner au village après le combat.
    /// </summary>
    public void RetourVillage()
    {
        SceneManager.LoadScene("SceneVillage");
    }
}
