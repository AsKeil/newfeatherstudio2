﻿

namespace Models
{
    /// <summary>
    /// DEF-5-10 Village
    /// Entité qui contient les ressources accumulée et les informations relative au village.
    /// </summary>
    public class VillageEntity
    {
        public RaceEntity race;
        public string carte; // N'est pas implémenté.
        public string nom;
        public int nombreDeJours;
        public int nourriture;
        public int bois;
        public int pierre;
        public int or;
        public int villageoisTotal;
        public int villageoisDisponible;

        /// <summary>
        /// Constructeur pour une nouvelle partie.
        /// </summary>
        /// <param name="race">La race de la nouvelle partie.</param>
        /// <param name="nom">Le nom de la nouvelle partie.</param>
        public VillageEntity(RaceEntity race, string nom)
        {
            this.race = race;
            this.carte = ""; // N'est pas implémenté.
            this.nom = nom;
            this.nombreDeJours = 0;
            this.nourriture = 100;
            this.bois = 100;
            this.pierre = 100;
            this.or = 100;
            this.villageoisTotal = 3;
            this.villageoisDisponible = 3;
        }

        /// <summary>
        /// Constructeur pour un village chargée.
        /// </summary>
        /// <param name="race">La race du village.</param>
        /// <param name="carte">La carte du village.</param>
        /// <param name="nom">Le nom du village</param>
        /// <param name="nombreDeJours">Le nombre de jour depuis le début de la partie.</param>
        /// <param name="nourriture">La nourriture accumulé.</param>
        /// <param name="bois">Le bois accumulé.</param>
        /// <param name="pierre">La pierre accumulé.</param>
        /// <param name="or">L'or accumulé.</param>
        /// <param name="villageoisTotal">Le total de villageois dans le village.</param>
        /// <param name="VillageoisDisponible">Le total de villageois disponibles.</param>
        public VillageEntity(RaceEntity race, string carte, string nom,
            int nombreDeJours, int nourriture, int bois, int pierre,
            int or, int villageoisTotal, int VillageoisDisponible)
        {
            this.race = race;
            this.carte = carte;
            this.nom = nom;
            this.nombreDeJours = nombreDeJours;
            this.nourriture = nourriture;
            this.bois = bois;
            this.pierre = pierre;
            this.or = or;
            this.villageoisTotal = villageoisTotal;
            this.villageoisDisponible = VillageoisDisponible;
        }
    }
}
