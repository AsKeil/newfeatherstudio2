﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;
using TMPro;
using NUnit.Framework;
using System.Collections.Generic;
using Models;

namespace Tests
{
    public class TestsMenu
    {
        public GameObject testObject;

        public TMP_InputField VillageinputField;

        public TMP_Dropdown RaceDropdown;

        public Button buttonCreer;


        /// <summary>
        /// Permet de créer un environnement semi-indépendant du jeu en soit
        /// pour pouvoir tester le script TestsMenu.
        /// </summary>
        void CreerEnvironnementTest()
        {
            testObject = new GameObject();
            testObject.AddComponent<Nouvellepartie>();

            GameObject objnomvillage = new GameObject();
            objnomvillage.AddComponent<TMP_InputField>();

            VillageinputField = objnomvillage.GetComponent<TMP_InputField>();
            VillageinputField.text = "LeNomDuVil";
            testObject.GetComponent<Nouvellepartie>().VillageinputField = VillageinputField;

            GameObject objrace = new GameObject();

            objrace.AddComponent<TMP_Dropdown>();
            RaceDropdown = objrace.GetComponent<TMP_Dropdown>();
            RaceDropdown.ClearOptions();

            List<string> optionslist = new List<string>();

            foreach (RaceEntity race in ApplicationData.ListeRacesE)
            {

                optionslist.Add(race.nomRace);

            }
            RaceDropdown.AddOptions(optionslist);

            RaceDropdown.value = 2;
            testObject.GetComponent<Nouvellepartie>().RaceDropdown = RaceDropdown;


            GameObject objbutton = new GameObject();
            objbutton.AddComponent<Button>();
            buttonCreer = objbutton.GetComponent<Button>();
            buttonCreer.interactable = false;
            testObject.GetComponent<Nouvellepartie>().buttonCreer = buttonCreer;

        }

        [UnityTest]
        public IEnumerator TestTrouverNomVillage()
        {
            CreerEnvironnementTest();
            string testnom;
            testnom = testObject.GetComponent<Nouvellepartie>().TrouverNomVillage();
            yield return null;
            Assert.AreEqual("LeNomDuVil", testnom);
        }

        [UnityTest]
        public IEnumerator TestTrouverRace()
        {
            CreerEnvironnementTest();
            RaceEntity testrace;
            testrace = testObject.GetComponent<Nouvellepartie>().TrouverRace();
            string nomRaceOrc = RaceDropdown.options[2].text;
            yield return null;
            Assert.AreEqual(nomRaceOrc, testrace.nomRace);
        }

    }
}
