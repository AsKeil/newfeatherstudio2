﻿

namespace Models
{
    /// <summary>
    /// DEF-13-12 Equipement
    /// Arme ou armure qui peut être équipé par un combattant.
    /// </summary>
    public class EquipementsEntity
    {
        public string type;
        public string nom;
        public int pointsDAttaque;
        public int pointsDeDefense;

        /// <summary>
        /// Constructeur pour les équipements venant de l'encyclopédie.
        /// </summary>
        /// <param name="type">Le type d'équipement</param>
        /// <param name="nom">Le nom de l'équipement</param>
        /// <param name="pointsDAttaque">Les points d'attaque de l'équipement</param>
        /// <param name="pointsDeDefense">Les points de defense de l'équipement</param>
        public EquipementsEntity(string type, string nom,
            int pointsDAttaque, int pointsDeDefense)
        {
            this.type = type;
            this.nom = nom;
            this.pointsDAttaque = pointsDAttaque;
            this.pointsDeDefense = pointsDeDefense;
        }
    }
}
