﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Variable qui spécifie quel événement il est.
/// Associer au prefab Événement.
/// </summary>
public class EvenementVariable : MonoBehaviour
{
    [HideInInspector]
    public Models.EvenementEntity evenement;
}
