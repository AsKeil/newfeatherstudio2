using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class initialisant les mouvements d'un allié.
/// </summary>
public class MouvementAllies : MouvementCombatants
{
    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {   if (GetComponent<ActionAllies>().tourCombatantActif)
        {
            if(GetTuilecourante()==null)
            {
                SetTuileCourrante();
            }
            if (!enMouvement && !finMouvement)
            {
                if (tuileSelectionable.Count==0)               
                    TrouverTuileSelectionable();
                VerifierSouris();
            }
            else if (!finMouvement)
            {
                DeplacementCombattant();
            }
            if (finMouvement)
            {
                SetTuileCourrante();
            }
        }     
    }
    /// <summary>
    /// Fonction permettant de détecter et d'enregistrer
    /// les clics de souris sur la grille. 
    /// </summary>
    void VerifierSouris()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.GetComponent<Tuile>())
                {
                    Tuile t = hit.collider.GetComponent<Tuile>();
                    if (t.estSelectionable)
                    {
                        AtteindreDestination(t);
                    }
                }
            }
        }
    }
}
