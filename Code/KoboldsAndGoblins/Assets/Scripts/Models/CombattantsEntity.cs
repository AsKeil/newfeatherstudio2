﻿using System;

namespace Models
{
    /// <summary>
    /// DEF-5-2 Combattant
    /// Représente un personnage qui peut se battre durant la phase de combat.
    /// </summary>
    public class CombattantsEntity
    {
        public RaceEntity race;
        public string nom;
        public int pointsDeVieMaximum;
        public int pointsDeVieActuels;
        public int pointsDAttaque;
        public int pointsDeDefense;
        public int porteeVision;
        public int porteeDeplacement;
        public string image;
        public EquipementsEntity arme;
        public EquipementsEntity armure;
        public int groupeEvenement; // N'est pas implémenté. // 0 ou null quand le combattant est dans le village.

        /// <summary>
        /// Constructeur vide.
        /// </summary>
        public CombattantsEntity()
        {

        }

        /// <summary>
        /// Combattant générique.
        /// </summary>
        /// <param name="race"> La race du combattant. </param>
        /// <param name="arme"> L'arme du combattant. Null s'il n'en a pas. </param>
        /// <param name="armure"> L'armure du combattant. Null s'il n'en a pas. </param>
        public CombattantsEntity(RaceEntity race, EquipementsEntity arme, EquipementsEntity armure)
        {
            string nombreAleatoire = ApplicationData.random.Next(1000, 9999).ToString();

            this.race = race;
            this.nom = "Combattant : " + nombreAleatoire;
            this.pointsDeVieMaximum = 10;
            this.pointsDeVieActuels = 10;
            this.pointsDAttaque = 5;
            this.pointsDeDefense = 0;
            this.porteeVision = 5;
            this.porteeDeplacement = 5;
            this.image = "Image test : " + DateTime.Now;
            this.arme = arme;
            this.armure = armure;
            this.groupeEvenement = 0;

            CombattantValide();
        }

        /// <summary>
        /// Valide les champs du combattant créé pour qu'ils soient valides.
        /// Génère une exception si la race du combattant est nulle.
        /// </summary>
        private void CombattantValide()
        {
            if (this.race == null)
            {
                throw new ArgumentNullException("La race envoyé en argument est nulle.");
            }
            if (!(1 <= nom.Length || this.nom.Length <= 20))
            {
                this.nom = "Bob";
            }
            if (this.pointsDeVieMaximum < 1)
            {
                this.pointsDeVieMaximum = 1;
            }
            if (this.pointsDeVieActuels < 0)
            {
                this.pointsDeVieActuels = 0;
            }
            else if (this.pointsDeVieMaximum < this.pointsDeVieActuels)
            {
                this.pointsDeVieActuels = this.pointsDeVieMaximum;
            }
            if (this.pointsDAttaque < 1)
            {
                this.pointsDAttaque = 1;
            }
            if (this.pointsDeDefense < 0)
            {
                this.pointsDeDefense = 0;
            }
            if (this.porteeVision < 1)
            {
                this.porteeVision = 1;
            }
            if (this.porteeDeplacement < 0)
            {
                this.porteeDeplacement = 0;
            }
        }
    }
}
