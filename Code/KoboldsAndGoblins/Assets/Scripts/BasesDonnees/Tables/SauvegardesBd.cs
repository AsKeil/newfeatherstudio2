﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "Sauvegardes" dans la base de données "Sauvegarde".
    /// </summary>
    public class SauvegardesBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champNomVillage = "nomVillage";
        private const string _champDateHeure = "dateHeure";
        private const string _clePrimaire = _champId;

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdSauvegarde"> Le nom de la BD de sauvegardes.</param>
        public SauvegardesBd(string nomBdSauvegarde) : base(nomBdSauvegarde, "Sauvegardes")
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champNomVillage + " TEXT, "
                + _champDateHeure + " TEXT DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME')), "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            ;
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="sauvegarde"> L'entité à ajouter dans la BD.</param>
        public void AddData(SauvegardeEntity sauvegarde)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champNomVillage
                + " ) "

                + "VALUES (@nomVillage)";
            AddParameterWithValue(dbcmd, "@nomVillage", sauvegarde._nomVillage);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<SauvegardeEntity> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<SauvegardeEntity> liste = SauvegardesEntityParser(reader);
            reader.Close();
            return liste;
        }

        /// <summary>
        /// Retourne une entité à partir de son ID.
        /// </summary>
        /// <param name="id"> Le ID de l'entité cherchée.</param>
        /// <returns> L'entité cherchée.</returns>
        public SauvegardeEntity GetEntityFromId(string id)
        {
            IDataReader reader = GetDataFromString(_champId, id);
            List<SauvegardeEntity> liste = SauvegardesEntityParser(reader);
            reader.Dispose();
            return liste[0];
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<SauvegardeEntity> SauvegardesEntityParser(IDataReader reader)
        {
            List<SauvegardeEntity> liste = new List<SauvegardeEntity>();
            while (reader.Read())
            {
                SauvegardeEntity sauvegarde = new SauvegardeEntity(reader[1].ToString(),
                                                                    reader[2].ToString());
                liste.Add(sauvegarde);
            }
            return liste;
        }

        /// <summary>
        /// Retourne un ID à partir d'une entité.
        /// </summary>
        /// <param name="sauvegarde"> L'entité dont on cherche le ID.</param>
        internal string GetIdFromEntity(SauvegardeEntity sauvegarde)
        {
            IDataReader reader = GetDataFromString(_champDateHeure, sauvegarde._dateHeure);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }

        /// <summary>
        /// Supprime une entrée de sauvegarde de la BD à partir d'une entité de sauvegarde.
        /// </summary>
        /// <param name="sauvegarde"> La sauvegarde à supprimer. </param>
        public void DeleteFromEntity(SauvegardeEntity sauvegarde)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "DELETE FROM " + nom_table
                + " WHERE " + _champDateHeure
                + " LIKE @_dateHeure"
                + " AND " + _champNomVillage
                + " LIKE @_nomVillage";
            AddParameterWithValue(dbcmd, "@_dateHeure", sauvegarde._dateHeure);
            AddParameterWithValue(dbcmd, "@_nomVillage", sauvegarde._nomVillage);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }
    }
}
