﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    /// <summary>
    /// Espace entre chaque point de la grille.
    /// </summary>
    [SerializeField]
    private float grandeur = 1f;

    /// <summary>
    /// Permet de trouver le point le plus proche de la position entrée.
    /// Cela sera utilisé avec la position du curseur de la souris lorsque
    /// le joueur fait un clique-gauche de la souris.
    /// </summary>
    /// <param name="position">Coordonnées de la souris.</param>
    /// <returns>Les coordonnées du point le plus proche sur la grille des coordonnées entrées.</returns>
    public Vector3 GetNearestPointOnGrid(Vector3 position)
    {
        position -= transform.position;

        int xCount = Mathf.RoundToInt(position.x / grandeur);
        int yCount = Mathf.RoundToInt(position.y / grandeur);
        int zCount = Mathf.RoundToInt(position.z / grandeur);

        Vector3 result = new Vector3(
            (float)xCount * grandeur,
            (float)yCount * grandeur,
            (float)zCount * grandeur);

        result += transform.position;

        return result;
    }

    /// <summary>
    /// Permet d'avoir un repère visuel en mode Scène.
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        for (float x = 0; x < 40; x += grandeur)
        {
            for (float z = 0; z < 40; z += grandeur)
            {
                var point = GetNearestPointOnGrid(new Vector3(x, 0f, z));
                Gizmos.DrawSphere(point, 0.1f);
            }

        }
    }
    /// <summary>
    /// Permet d'aller chercher la valeur de la variable grandeur.
    /// </summary>
    /// <returns> La distance entre deux points de la grille </returns>
    public float GetGrandeur() 
    {
        return grandeur;
    }

    /// <summary>
    /// Permet de modifier la variable size.
    /// </summary>
    /// <param name="size"> La nouvelle valeur de la variable grandeur </param>
    public void SetGrandeur(float grandeur)
    {
        this.grandeur = grandeur;
    }
}
