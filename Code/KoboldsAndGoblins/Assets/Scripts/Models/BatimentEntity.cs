﻿

namespace Models
{
    /// <summary>
    /// DEF-5-5 Bâtiment
    /// Structure du village fabriquée avec les ressources et pouvant servir à améliorer le village.
    /// Ils peuvent se retrouver dans la phase de combat.
    /// </summary>
    public class BatimentEntity
    {
        public string nom;
        public string description;
        public string image;
        
        public int coutNourriture;
        public int coutBois;
        public int coutPierre;
        public int coutOr;
        public int nombreDevillageoisMaximum;
        public int nombreDeVillageoisAssignes;

        public int pointsDeDefense;
        public int pointsDeVieMaximum;
        public int pointsDeVieActuels;

        public int positionX;
        public int positionY;

        /// <summary>
        /// Constructeur pour un bâtiment venant de l'encyclopédie.
        /// </summary>
        /// <param name="nom">Le nom du bâtiment</param>
        /// <param name="description">La description du bâtiment</param>
        /// <param name="image">L'image du bâtiment</param>
        /// <param name="coutNourriture">Le coût en nourriture du bâtiment</param>
        /// <param name="coutBois">Le coût en bois du bâtiment</param>
        /// <param name="coutPierre">Le coût en pierre du bâtiment</param>
        /// <param name="coutOr">Le coût en or du bâtiment</param>
        /// <param name="nombreDevillageoisMaximum">Le nombre de villagois maximum du bâtiment</param>
        /// <param name="pointsDeVieMaximum">Les points de vie maximum du bâtiment</param>
        /// <param name="pointsDeDefense">Les points de defense du bâtiment</param>
        public BatimentEntity(string nom, string description, string image,
            int coutNourriture, int coutBois, int coutPierre, int coutOr,
            int nombreDevillageoisMaximum, int pointsDeVieMaximum,
            int pointsDeDefense)
        {
            this.nom = nom;
            this.description = description;
            this.image = image;
            this.coutNourriture = coutNourriture;
            this.coutBois = coutBois;
            this.coutPierre = coutPierre;
            this.coutOr = coutOr;
            this.nombreDevillageoisMaximum = nombreDevillageoisMaximum;
            this.pointsDeDefense = pointsDeDefense;
            this.pointsDeVieMaximum = pointsDeVieMaximum;
        }

        /// <summary>
        /// Constructeur pour un bâtiment construit.
        /// </summary>
        /// <param name="batimentE">Le type de bâtiment</param>
        /// <param name="nombreDeVillageoisAssignes">Le nombre de villagois assigne au bâtiment</param>
        /// <param name="pointsDeVieActuels">Le nombre de points de vie actuels du bâtiment</param>
        /// <param name="positionX">La position en x du bâtiment</param>
        /// <param name="positionY">La position en y du bâtiment</param>
        public BatimentEntity(BatimentEntity batimentE, int nombreDeVillageoisAssignes,
            int pointsDeVieActuels, int positionX, int positionY)
        {
            this.nom = batimentE.nom;
            this.description = batimentE.description;
            this.image = batimentE.image;
            this.coutNourriture = batimentE.coutNourriture;
            this.coutBois = batimentE.coutBois;
            this.coutPierre = batimentE.coutPierre;
            this.coutOr = batimentE.coutOr;
            this.nombreDevillageoisMaximum = batimentE.nombreDevillageoisMaximum;
            this.pointsDeVieMaximum = batimentE.pointsDeVieMaximum;
            this.pointsDeDefense = batimentE.pointsDeDefense;

            this.nombreDeVillageoisAssignes = nombreDeVillageoisAssignes;
            this.pointsDeVieActuels = pointsDeVieActuels;
            this.positionX = positionX;
            this.positionY = positionY;
        }
    }
}
