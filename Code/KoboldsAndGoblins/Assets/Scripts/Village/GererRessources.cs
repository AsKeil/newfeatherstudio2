﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GererRessources : MonoBehaviour
{
    /// <summary>
    /// Vérifie si le coût des ressources de nourriture, de bois, de pierre et d'or 
    /// entrées est inférieur aux ressources disponibles.
    /// </summary>
    /// <param name="nourriture">Le coût de nourriture</param>
    /// <param name="bois">Le coût de bois</param>
    /// /// <param name="pierre">Le coût de pierre</param>
    /// <param name="or">Le coût d'or</param>
    public bool VerifierCoutRessources(int nourriture, int bois, int pierre, int or)
    {
        if (ApplicationData.village.nourriture < nourriture ||
            ApplicationData.village.bois < bois ||
            ApplicationData.village.pierre < pierre ||
            ApplicationData.village.or < or)
            return false;
        else
            return true;
    }

    /// <summary>
    /// Vérifie si le coût de nourriture entrée est inférieur aux ressources disponibles.
    /// </summary>
    /// <param name="nourriture">Le coût de nourriture</param>
    public bool VerifierCoutNourriture(int nourriture)
    {
        if (ApplicationData.village.nourriture < nourriture)
            return false;
        else
            return true;
    }

    /// <summary>
    /// Vérifie si le coût de bois entré est inférieur aux ressources disponibles.
    /// </summary>
    /// <param name="bois">Le coût de bois</param>
    public bool VerifierCoutBois(int bois)
    {
        if (ApplicationData.village.bois < bois)
            return false;
        else
            return true;
    }

    /// <summary>
    /// Vérifie si le coût de pierre entré est inférieur aux ressources disponibles.
    /// </summary>
    /// /// <param name="pierre">Le coût de pierre</param>
    public bool VerifierCoutPierre(int pierre)
    {
        if (ApplicationData.village.pierre < pierre)
            return false;
        else
            return true;
    }

    /// <summary>
    /// Vérifie si le coût d'or entré est inférieur aux ressources disponibles.
    /// </summary>
    /// <param name="or">Le coût d'or</param>
    public bool VerifierCoutOr(int or)
    {
        if (ApplicationData.village.or < or)
            return false;
        else
            return true;
    }

    /// <summary>
    /// Diminue les ressources disponibles par le coût entré après avoir
    /// vérifier si le coût des ressources de nourriture, de bois, de 
    /// pierre et d'or entrées est inférieur aux ressources disponibles.
    /// </summary>
    /// <param name="nourriture">Le coût de nourriture</param>
    /// <param name="bois">Le coût de bois</param>
    /// /// <param name="pierre">Le coût de pierre</param>
    /// <param name="or">Le coût d'or</param>
    public void EnleverRessource(int nourriture, int bois, int pierre, int or)
    {
        if (VerifierCoutRessources(nourriture, bois, pierre, or))
        {
            ApplicationData.village.nourriture -= nourriture;
            ApplicationData.village.bois -= bois;
            ApplicationData.village.pierre -= pierre;
            ApplicationData.village.or -= or;
        }
        else
        {
            Debug.Log("Pas assez de ressources.");
        }
    }
}
