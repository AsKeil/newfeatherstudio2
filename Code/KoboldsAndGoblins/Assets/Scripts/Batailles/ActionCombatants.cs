﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class initialisant les valeurs de départ d'un combattant(point de vie, point d'attaque, etc.)
/// Class gérant uniquement la phase d'attaque d'un combattant.
/// </summary>
public class ActionCombatants : MonoBehaviour
{
    // Caractéristique des combattants.
    public int pointsDAttaque;
    public int pointsDeDefense;
    public int pointsDeVieActuels;
    public int pointsDeVieMaximum;
    public string nom;
    public int porteDeDeplacement;
    public bool estMort = false;
    
    //Boolean permettant de savoir si ses le tour du combattant.
    public bool tourCombatantActif = false;
    public bool attaqueEffectuer = false;

    //Position adjacente d'un combattant.
    public List<Vector3> listPositionAdjascente = new List<Vector3>();
    
    public Vector3 positioAdjascenteGauche;
    public Vector3 positioAdjascenteDroite;
    public Vector3 positioAdjascenteAvant;
    public Vector3 positioAdjascenteArriere;

    /// <summary>
    /// Initialise les valeurs de base des combattants.
    /// </summary>
    public void Init()
    {
        porteDeDeplacement = 3;
        pointsDAttaque = 3;
        pointsDeDefense = 1;
        pointsDeVieMaximum = 6;
        pointsDeVieActuels = pointsDeVieMaximum;
        GestionaireTour.AjouterCombatant(this);

    }

    /// <summary>
    /// Fonction servant à trouver les positions adjacentes du combattant.
    /// </summary>
    public void PositionAdjacente()
    {
        listPositionAdjascente = new List<Vector3>();

        positioAdjascenteGauche = new Vector3(transform.position.x - 1, 1.4f, transform.position.z);
        listPositionAdjascente.Add(positioAdjascenteGauche);
        
        positioAdjascenteDroite = new Vector3(transform.position.x + 1, 1.4f, transform.position.z);
        listPositionAdjascente.Add(positioAdjascenteDroite);
        
        positioAdjascenteAvant = new Vector3(transform.position.x, 1.4f, transform.position.z + 1);
        listPositionAdjascente.Add(positioAdjascenteAvant);
        
        positioAdjascenteArriere = new Vector3(transform.position.x , 1.4f, transform.position.z - 1);
        listPositionAdjascente.Add(positioAdjascenteArriere);
    }
    /// <summary>
    ///  Fonction mettant la variable estConbatantTour à true.
    /// </summary>
    public void DebutCombatantTour()
    {
        if(!estMort)
            tourCombatantActif = true;       
    }

    /// <summary>
    /// Fonction mettant la variable tourCombatantActif à false.
    /// </summary>
    public void FinCombatantTour()
    {
        tourCombatantActif = false;
        GetComponent<MouvementCombatants>().finMouvement = false;
        attaqueEffectuer = false;
        
    }
   
}
