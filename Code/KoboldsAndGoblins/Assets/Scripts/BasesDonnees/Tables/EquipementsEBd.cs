﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "EquipementsE" dans la base de données "Encyclopedie".
    /// </summary>
    public class EquipementsEBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champType = "type";
        private const string _champNom = "nom";
        private const string _champPointsDAttaque = "pointsDAttaque";
        private const string _champPointsDeDefense = "pointsDeDefense";
        private const string _clePrimaire = "id";

        public string NomTable { get { return nom_table; } }
        public string ChampId { get { return _champId; } }
        public string Nom { get { return _champNom; } }

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD encyclopédie.</param>
        public EquipementsEBd(string nomBdEncyclopedie) : base(nomBdEncyclopedie, "EquipementsE")
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champType + " TEXT NOT NULL, "
                + _champNom + " TEXT NOT NULL, "
                + _champPointsDAttaque + " INTEGER NOT NULL, "
                + _champPointsDeDefense + " INTEGER NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="batiment"> L'entité à ajouter dans la BD.</param>
        public void AddData(EquipementsEntity equipement)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champType + ", "
                + _champNom + ", "
                + _champPointsDAttaque + ", "
                + _champPointsDeDefense
                + " ) "

                + "VALUES ( @type"
                + ", @nom"
                + ", @pointsDAttaque"
                + ", @pointsDeDefense"
                + " )";
            AddParameterWithValue(dbcmd, "@type", equipement.type);
            AddParameterWithValue(dbcmd, "@nom", equipement.nom);
            AddParameterWithValue(dbcmd, "@pointsDAttaque", equipement.pointsDAttaque);
            AddParameterWithValue(dbcmd, "@pointsDeDefense", equipement.pointsDeDefense);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entitée pour chacune des entrées de la table
        /// </summary>
        public List<EquipementsEntity> GetListeEntity()
        {
            List<EquipementsEntity> liste = new List<EquipementsEntity>();
            System.Data.IDataReader reader = GetAllData();
            while (reader.Read())
            {
                EquipementsEntity equipement = new EquipementsEntity(
                    reader[1].ToString(),
                    reader[2].ToString(),
                    int.Parse(reader[3].ToString()),
                    int.Parse(reader[4].ToString()));
                liste.Add(equipement);
            }
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table qui ont un certain type.
        /// </summary>
        /// <param name="type"> Le type de l'équipement cherché. ("arme","armure")</param>
        internal List<EquipementsEntity> GetListeEntity(string type)
        {
            IDataReader reader = GetDataFromString(_champType, type);
            List<EquipementsEntity> liste = EquipementsEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Retourne une entitée à partir de son ID.
        /// </summary>
        public EquipementsEntity GetEntityFromId(string id)
        {
            IDataReader reader = GetDataFromString(_champId, id);
            List<EquipementsEntity> liste = EquipementsEntityParser(reader);
            reader.Dispose();
            return liste[0];
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<EquipementsEntity> EquipementsEntityParser(IDataReader reader)
        {
            List<EquipementsEntity> liste = new List<EquipementsEntity>();
            while (reader.Read())
            {
                EquipementsEntity equipement = new EquipementsEntity(
                    reader[1].ToString(),
                    reader[2].ToString(),
                    int.Parse(reader[3].ToString()),
                    int.Parse(reader[4].ToString()));
                liste.Add(equipement);
            }
            return liste;
        }

        /// <summary>
        /// Retourne une entité à partir de son ID.
        /// </summary>
        /// <param name="id"> Le ID de l'entité cherchée.</param>
        /// <returns> L'entité cherchée.</returns>
        internal string GetIdFromEntity(EquipementsEntity equipement)
        {
            IDataReader reader = GetDataFromString(_champNom, equipement.nom);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }

        /// <summary>
        /// Retourne le ID d'une entitée à partir de son nom.
        /// </summary>
        /// <param name="nom"> Le nom de l'entité dont on cherche le ID.</param>
        /// <returns> Le ID de l'entitée.</returns>
        internal string GetIdFromNom(string nom)
        {
            IDataReader reader = GetDataFromString(_champNom, nom);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }
    }
}
