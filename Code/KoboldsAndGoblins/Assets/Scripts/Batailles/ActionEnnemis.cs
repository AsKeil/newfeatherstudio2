﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class gérant les actions des ennemis.
/// </summary>
public class ActionEnnemis : ActionCombatants
{
    public BarDeVie barDeVie;
    // Start is called before the first frame update
    void Start()
    {
        Init();
        if(barDeVie != null)
            barDeVie.SetPtVieTxt(pointsDeVieActuels);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<ActionEnnemis>().tourCombatantActif)
        {
            return;
        }
        else if (GetComponent<MouvementEnnemis>().finMouvement)
        {
            GestionaireTour.FinTour();
        }
        if (pointsDeVieActuels <= 0)
        {
            estMort = true;
        }
    }
    public void DomageRecus(int dmg)
    {
        int domageRecus = (dmg - pointsDeDefense);
        if (domageRecus > 0)
            pointsDeVieActuels -= domageRecus;
        if (barDeVie != null)
            barDeVie.ChangementPtVie(pointsDeVieActuels);
    }
}
