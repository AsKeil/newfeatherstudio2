﻿using Mono.Data.Sqlite;
using UnityEngine;
using System.Data;
using System.Data.Common;

namespace DataBank
{
    /// <summary>
    /// Une base pour communiquer avec une BD.
    /// </summary>
    public abstract class SqliteHelper
    {
        public string bdConnexionString;
        public string nom_table;
        public IDbConnection bdConnexion;

        /// <summary>
        /// Crée la BD si elle n'existe pas.
        /// Ouvre la connexion.
        /// </summary>
        /// <param name="nom_bd">Le nom la de la BD à ouvrir.</param>
        /// <param name="nomTable">Le nom la de la table de cette BD.</param>
        public SqliteHelper(string nom_bd, string nomTable)
        {
            nom_table = nomTable;
            bdConnexionString = "URI=file:" + Application.persistentDataPath + "/" + nom_bd;
            bdConnexion = new SqliteConnection(bdConnexionString);
            bdConnexion.Open();
        }

        /// <summary>
        /// Retourne un DbCommand avec la connexion vers la BD d'ouverte.
        /// </summary>
        public DbCommand GetDbCommand()
        {
            return (DbCommand)bdConnexion.CreateCommand();
        }

        /// <summary>
        /// Supprime un élément de la table à partir de son ID.
        /// </summary>
        /// <param name="id">Le id de l'entrée à supprimer</param>
        public void DeleteById(int id)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "DELETE FROM " + nom_table + " WHERE id = @id";

            DbParameter param = dbcmd.CreateParameter();
            param.ParameterName = "@id";
            param.Value = id;
            dbcmd.Parameters.Add(param);

            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Permet d'aller chercher des information d'une table ou un champ a une certaine valeur.
        /// </summary>
        /// <param name="nom_champ">Le nom du champ</param>
        /// <param name="valeur_cherchee">La valeur cherchée</param>
        /// <returns>Un reader des informations cherchées.</returns>
        public IDataReader GetDataFromString(string nom_champ, string valeur_cherchee)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "SELECT * FROM " + nom_table + " WHERE " + nom_champ + " = @valeur_cherchee";
            
            DbParameter param = dbcmd.CreateParameter();
            param.ParameterName = "@valeur_cherchee";
            param.Value = valeur_cherchee;
            dbcmd.Parameters.Add(param);

            IDataReader reader = dbcmd.ExecuteReader();
            dbcmd.Dispose();
            return reader;
        }

        /// <summary>
        /// Retourne toutes les informations d'une table.
        /// </summary>
        /// <returns>Un reader contenant toutes les informations d'une table.</returns>
        public IDataReader GetAllData()
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "SELECT * FROM " + nom_table;
            IDataReader reader = dbcmd.ExecuteReader();
            dbcmd.Dispose();
            return reader;
        }

        /// <summary>
        /// Retourne le dernier ID d'une table.
        /// </summary>
        /// <returns>Le ID de la dernière entrée de la table.</returns>
        public string GetLastId()
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "SELECT MAX(id) FROM " + nom_table;
            IDataReader reader = dbcmd.ExecuteReader();
            dbcmd.Dispose();
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }

        /// <summary>
        /// Retourne le nombre d'entrées dans une table.
        /// </summary>
        public int GetNumOfRows()
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "SELECT COUNT(*) FROM " + nom_table;
            IDataReader reader = dbcmd.ExecuteReader();
            dbcmd.Dispose();
            int nombreDentrees = int.Parse(reader[0].ToString());
            reader.Dispose();
            return nombreDentrees;
        }

        /// <summary>
        /// Ajoute un paramètre à une commande.
        /// </summary>
        /// <param name="dbcmd"> La commande qui requière un paramètre.</param>
        /// <param name="nomParam"> Le nom du paramètre: "@nomParam" </param>
        /// <param name="valeurParam"> La valeur de ce paramètre. </param>
        public static void AddParameterWithValue(DbCommand dbcmd, string nomParam, object valeurParam)
        {
            DbParameter param = dbcmd.CreateParameter();
            param.ParameterName = nomParam;
            param.Value = valeurParam;
            dbcmd.Parameters.Add(param);
        }

        /// <summary>
        /// Ferme la connexion de la BD manuellement.
        /// </summary>
        public void Close()
        {
            bdConnexion.Close();
            bdConnexion.Dispose();
        }

        /// <summary>
        /// Ferme la connexion de la BD automatiquement.
        /// </summary>
        ~SqliteHelper()
        {
            Close();
        }
    }
}
