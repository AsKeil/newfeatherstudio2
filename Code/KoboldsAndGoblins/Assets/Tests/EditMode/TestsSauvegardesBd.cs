﻿using System.Collections.Generic;
using System.IO;
using DataBank;
using Models;
using NUnit.Framework;

namespace Tests
{
    /// <summary>
    /// Classe de test pour SauvegardesBd.
    /// </summary>
    public class TestsSauvegardesBd : TestsBd
    {
        /// <summary>
        /// Valide qu'il est possible de créer la table.
        /// </summary>
        [Test]
        public void SauvegardeBdConstructeur()
        {
            // Mise en place
            SetApplicationDataTest(true);
            SauvegardesBd sauvegardesBd = new SauvegardesBd(ApplicationData.NomBdSauvegarde);
            string path = cheminBd + ApplicationData.NomBdSauvegarde;

            // Vérification
            Assert.IsTrue(File.Exists(path));

            // Nettoyage
            sauvegardesBd.Close();
            SupprimerBdTest();
            SetApplicationDataTest(false);
        }

        /// <summary>
        /// Valide qu'il est possible d'ajouter et lire une entrée dans la table.
        /// </summary>
        [Test]
        public void SauvegardeBdAddData()
        {
            // Mise en place
            SetApplicationDataTest(true);
            string nomVillageSauvegarde = "NomVillageSauvegarde";
            SauvegardeEntity sauvegardeEntity = new SauvegardeEntity(nomVillageSauvegarde);
            SauvegardesBd sauvegardesBd = new SauvegardesBd(ApplicationData.NomBdSauvegarde);

            // Vérification
            sauvegardesBd.AddData(sauvegardeEntity);
            List<SauvegardeEntity> listeSauvegardes = sauvegardesBd.GetListeEntity();
            Assert.AreEqual(listeSauvegardes[0]._nomVillage, nomVillageSauvegarde);

            // Nettoyage
            sauvegardesBd.Close();
            SupprimerBdTest();
            SetApplicationDataTest(false);
        }

        /// <summary>
        /// Valide qu'il est possible de retrouver une entrée dans la table à partir de son Id.
        /// </summary>
        [Test]
        public void SauvegardeBdGetEntityFromId()
        {
            // Mise en place
            SetApplicationDataTest(true);
            string nomVillageSauvegarde = "NomVillageSauvegarde";
            SauvegardeEntity sauvegardeEntity = new SauvegardeEntity(nomVillageSauvegarde);
            SauvegardesBd sauvegardesBd = new SauvegardesBd(ApplicationData.NomBdSauvegarde);
            sauvegardesBd.AddData(sauvegardeEntity);

            // Vérification
            SauvegardeEntity sauvegardeCharge = sauvegardesBd.GetEntityFromId("1");
            Assert.AreEqual(sauvegardeCharge._nomVillage, nomVillageSauvegarde);

            // Nettoyage
            sauvegardesBd.Close();
            SupprimerBdTest();
            SetApplicationDataTest(false);
        }

        /// <summary>
        /// Valide qu'il est possible de connaître le nombre d'entrée dans la table.
        /// </summary>
        [Test]
        public void SauvegardeBdGetNumOfRows()
        {
            // Mise en place
            SetApplicationDataTest(true);
            SauvegardeEntity sauvegardeEntity = new SauvegardeEntity("NomVillageSauvegarde");
            SauvegardesBd sauvegardesBd = new SauvegardesBd(ApplicationData.NomBdSauvegarde);

            // Vérification
            Assert.AreEqual(sauvegardesBd.GetNumOfRows(), 0);
            sauvegardesBd.AddData(sauvegardeEntity);
            Assert.AreEqual(sauvegardesBd.GetNumOfRows(), 1);

            // Nettoyage
            sauvegardesBd.Close();
            SupprimerBdTest();
            SetApplicationDataTest(false);
        }

        /// <summary>
        /// Valide qu'il est possible de supprimer une entrée à partir de l'entitée.
        /// Issue #17 : Suppression d'une save file ne fonctionne pas
        /// </summary>
        [Test]
        public void SauvegardeBdDeleteFromEntity()
        {
            // Mise en place
            SetApplicationDataTest(true);
            SauvegardeEntity sauvegardeEntity = new SauvegardeEntity("NomVillageSauvegarde");
            SauvegardesBd sauvegardesBd = new SauvegardesBd(ApplicationData.NomBdSauvegarde);
            sauvegardesBd.AddData(sauvegardeEntity);
            SauvegardeEntity sauvegardeAsupprimer = sauvegardesBd.GetEntityFromId("1");

            // Vérification
            Assert.AreEqual(sauvegardesBd.GetNumOfRows(), 1);
            sauvegardesBd.DeleteFromEntity(sauvegardeAsupprimer);
            Assert.AreEqual(sauvegardesBd.GetNumOfRows(), 0);

            // Nettoyage
            sauvegardesBd.Close();
            SupprimerBdTest();
            SetApplicationDataTest(false);
        }
    }
}
