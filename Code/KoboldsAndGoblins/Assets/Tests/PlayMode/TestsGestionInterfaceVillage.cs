﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestsGestionInterfaceVillage
    {
        public GameObject TestDetailCombattant, TestDetailEvenement, TestMenuConstruction, TestGameManager;
        public Models.CombattantsEntity TestCombattant;
        public Models.EvenementEntity TestEvenement;

        /// <summary>
        /// Permet de créer un environnement semi-indépendant du jeu en soit
        /// pour pouvoir tester le script GestionInterface.
        /// </summary>
        void CreerEnvironnementTest()
        {
            InitialiserTestDetailCombattant();
            InitialiserTestDetailEvenement();
            InitialiserTestCombattant();
            TestEvenement = new Models.EvenementEntity("NomTest", "DescriptionTest", "TexteAgrementTest");
            TestMenuConstruction = new GameObject();

            TestGameManager = new GameObject();
            TestGameManager.AddComponent<TestGestionInterface>();
            TestGameManager.GetComponent<TestGestionInterface>().DetailCombattant = TestDetailCombattant;
            TestGameManager.GetComponent<TestGestionInterface>().DetailEvenement = TestDetailEvenement;
            TestGameManager.GetComponent<TestGestionInterface>().MenuConstruction = TestMenuConstruction;
        }

        /// <summary>
        /// Permet d'initialiser le GameObject TestDetailCombattant.
        /// </summary>
        void InitialiserTestDetailCombattant()
        {
            TestDetailCombattant = new GameObject();
            GameObject Nom, Race, PointsdeVie, Attaque, Defense, Vision, Deplacement, Arme, Armure;
            // Nom
            Nom = new GameObject();
            Nom.name = "Nom";
            Nom.AddComponent<TextMeshProUGUI>();
            Nom.transform.SetParent(TestDetailCombattant.transform);
            // Race
            Race = new GameObject();
            Race.name = "Race";
            Race.AddComponent<TextMeshProUGUI>();
            Race.transform.SetParent(TestDetailCombattant.transform);
            // Points de Vie
            PointsdeVie = new GameObject();
            PointsdeVie.name = "PointsdeVie";
            PointsdeVie.AddComponent<TextMeshProUGUI>();
            PointsdeVie.transform.SetParent(TestDetailCombattant.transform);
            // Attaque
            Attaque = new GameObject();
            Attaque.name = "Attaque";
            Attaque.AddComponent<TextMeshProUGUI>();
            Attaque.transform.SetParent(TestDetailCombattant.transform);
            // Défense
            Defense = new GameObject();
            Defense.name = "Defense";
            Defense.AddComponent<TextMeshProUGUI>();
            Defense.transform.SetParent(TestDetailCombattant.transform);
            // Vision
            Vision = new GameObject();
            Vision.name = "Vision";
            Vision.AddComponent<TextMeshProUGUI>();
            Vision.transform.SetParent(TestDetailCombattant.transform);
            // Déplacement
            Deplacement = new GameObject();
            Deplacement.name = "Deplacement";
            Deplacement.AddComponent<TextMeshProUGUI>();
            Deplacement.transform.SetParent(TestDetailCombattant.transform);
            // Arme
            Arme = new GameObject();
            Arme.name = "Arme";
            Arme.AddComponent<TextMeshProUGUI>();
            Arme.transform.SetParent(TestDetailCombattant.transform);
            // Armure
            Armure = new GameObject();
            Armure.name = "Armure";
            Armure.AddComponent<TextMeshProUGUI>();
            Armure.transform.SetParent(TestDetailCombattant.transform);
        }

        void InitialiserTestDetailEvenement()
        {
            TestDetailEvenement = new GameObject();
            GameObject Nom, Description, TexteAgrement;
            // Nom
            Nom = new GameObject();
            Nom.name = "Nom";
            Nom.AddComponent<TextMeshProUGUI>();
            Nom.transform.SetParent(TestDetailEvenement.transform);
            // Nom
            Description = new GameObject();
            Description.name = "Description";
            Description.AddComponent<TextMeshProUGUI>();
            Description.transform.SetParent(TestDetailEvenement.transform);
            // Nom
            TexteAgrement = new GameObject();
            TexteAgrement.name = "TexteAgrement";
            TexteAgrement.AddComponent<TextMeshProUGUI>();
            TexteAgrement.transform.SetParent(TestDetailEvenement.transform);
        }

        /// <summary>
        /// Permet d'initialiser le Models.CombattantsEntity TestCombattant.
        /// </summary>
        void InitialiserTestCombattant()
        {
            TestCombattant = new Models.CombattantsEntity();
            TestCombattant.nom = "Bob";
            TestCombattant.race = new Models.RaceEntity("Joe");
            TestCombattant.pointsDeVieMaximum = 10;
            TestCombattant.pointsDeVieActuels = 9;
            TestCombattant.pointsDAttaque = 5;
            TestCombattant.pointsDeDefense = 4;
            TestCombattant.porteeVision = 3;
            TestCombattant.porteeDeplacement = 2;
            TestCombattant.arme = null;
            TestCombattant.armure = null;
        }

        /// <summary>
        /// Test pour vérifier si l'objet TestDetailCombattant est afficher au début de la scène.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierSiDetailCombattantEstAfficherEnDebutDeScene()
        {
            CreerEnvironnementTest();
            yield return null;
            Assert.IsFalse(TestDetailCombattant.activeSelf);
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour vérifier si la méthode GererAffichageDetailCombattant() modifie les
        /// données écrites dans l'objet TestDetailCombattant pour lesvaleurs de 
        /// TestCombattant correspondante et si elle change le status entre actif et 
        /// désactif de TestDetailCombattant.
        /// </summary>
        [UnityTest]
        public IEnumerator TestGererAffichageDetailCombattant()
        {
            CreerEnvironnementTest();
            yield return null;
            TestGameManager.GetComponent<TestGestionInterface>().GererAffichageDetailCombattant(TestCombattant);
            yield return null;
            Assert.AreEqual(TestDetailCombattant.transform.Find("Nom").GetComponent<TextMeshProUGUI>().text, "Bob");
            Assert.AreEqual(TestDetailCombattant.transform.Find("Race").GetComponent<TextMeshProUGUI>().text, "Joe");
            Assert.AreEqual(TestDetailCombattant.transform.Find("PointsdeVie").GetComponent<TextMeshProUGUI>().text, "10");
            Assert.AreEqual(TestDetailCombattant.transform.Find("Attaque").GetComponent<TextMeshProUGUI>().text, "5");
            Assert.AreEqual(TestDetailCombattant.transform.Find("Defense").GetComponent<TextMeshProUGUI>().text, "4");
            Assert.AreEqual(TestDetailCombattant.transform.Find("Vision").GetComponent<TextMeshProUGUI>().text, "3");
            Assert.AreEqual(TestDetailCombattant.transform.Find("Deplacement").GetComponent<TextMeshProUGUI>().text, "2");
            Assert.AreEqual(TestDetailCombattant.transform.Find("Arme").GetComponent<TextMeshProUGUI>().text, "Aucune");
            Assert.AreEqual(TestDetailCombattant.transform.Find("Armure").GetComponent<TextMeshProUGUI>().text, "Aucune");
            Assert.IsTrue(TestDetailCombattant.activeSelf);
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour vérifier si l'objet TestDetailEvenement est afficher au début de la scène.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierSiDetailEvenementEstAfficherEnDebutDeScene()
        {
            CreerEnvironnementTest();
            yield return null;
            Assert.IsFalse(TestDetailEvenement.activeSelf);
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour vérifier si l'objet TestDetailEvenement est afficher au début de la scène.
        /// </summary>
        [UnityTest]
        public IEnumerator TestGererAffichageDetailEvenement()
        {
            CreerEnvironnementTest();
            yield return null;
            TestGameManager.GetComponent<TestGestionInterface>().GererAffichageDetailEvenement(TestEvenement);
            yield return null;
            Assert.AreEqual(TestDetailEvenement.transform.Find("Nom").GetComponent<TextMeshProUGUI>().text, "NomTest");
            Assert.AreEqual(TestDetailEvenement.transform.Find("Description").GetComponent<TextMeshProUGUI>().text, "DescriptionTest");
            Assert.AreEqual(TestDetailEvenement.transform.Find("TexteAgrement").GetComponent<TextMeshProUGUI>().text, "TexteAgrementTest");
            Assert.IsTrue(TestDetailEvenement.activeSelf);
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour vérifier si l'objet TestMenuConstruction est afficher au début de la scène.
        /// </summary>
        [UnityTest]
        public IEnumerator TestVerifierSiMenuConstructionEstAfficherEnDebutDeScene()
        {
            CreerEnvironnementTest();
            yield return null;
            Assert.IsFalse(TestMenuConstruction.activeSelf);
            ApplicationData.PartieNull();
        }

        /// <summary>
        /// Test pour vérifier si la méthode GererAffichageMenuConstruction modifie bien
        /// l'état de l'objet TestMenuConstruction.
        /// </summary>
        [UnityTest]
        public IEnumerator TestGererAffichageMenuConstruction()
        {
            CreerEnvironnementTest();
            yield return null;
            TestGameManager.GetComponent<TestGestionInterface>().GererAffichageMenuConstruction();
            yield return null;
            Assert.IsTrue(TestMenuConstruction.activeSelf);
            ApplicationData.PartieNull();
        }
    }

    /// <summary>
    /// Cette classe hérite de GestionInterface et est uniquement utilisé dans le but de
    /// tester la classe GestionInterface dans un environnement semi-indépendant.
    /// </summary>
    class TestGestionInterface : GestionInterface
    {
        protected override void Start()
        {
            GameManager = GameObject.Find("GameManager");
            DetailCombattant.SetActive(false);
            DetailEvenement.SetActive(false);
            MenuConstruction.SetActive(false);
        }

        protected override void Update()
        {
            
        }
    }
}
