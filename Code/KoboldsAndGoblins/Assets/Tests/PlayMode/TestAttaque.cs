﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{   
    public class TestAttaque
    {
        /// <summary>
        /// Test verifiant que l'allié attaque belle et bien l'ennemi.
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator TestAttaqueSimplePass()
        {
            var gestionairTour = GameObject.Instantiate(Resources.Load("Prefabs/Gestionaire_de_tour") as GameObject);
            var allie = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Allie") as GameObject);
            var ennemi = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Ennemi") as GameObject);
 
            ennemi.GetComponent<MouvementEnnemis>().enabled = false;
            allie.GetComponent<MouvementAllies>().enabled = false;
            
            yield return null;
            allie.GetComponent<ActionAllies>().Attaquer(ennemi.GetComponent<ActionCombatants>(),
                                        allie.GetComponent<ActionAllies>().pointsDAttaque);
            Assert.IsTrue(ennemi.GetComponent<ActionEnnemis>().pointsDeVieActuels < ennemi.GetComponent<ActionEnnemis>().pointsDeVieMaximum);

        }
        /// <summary>
        /// Test servant a s'assurer que l'allié n'envoie pas d'attaque negative.
        /// Si le test est reussi cela signifie que l'ennemi n'a recus aucun dégats.
        /// Si échouer cela signifi que l'ennemi à reçus des point de vie suplémentaire.
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator TestAttaqueNonNegatif()
        {
            var gestionairTour = GameObject.Instantiate(Resources.Load("Prefabs/Gestionaire_de_tour") as GameObject);
            var allie = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Allie") as GameObject);
            var ennemi = GameObject.Instantiate(Resources.Load("Prefabs/Combatant_Ennemi") as GameObject);

            ennemi.GetComponent<MouvementEnnemis>().enabled = false;
            allie.GetComponent<MouvementAllies>().enabled = false;
            yield return null;
            allie.GetComponent<ActionAllies>().pointsDAttaque = 0;
            allie.GetComponent<ActionAllies>().Attaquer(ennemi.GetComponent<ActionCombatants>(),
                                        allie.GetComponent<ActionAllies>().pointsDAttaque);
            Assert.IsTrue(ennemi.GetComponent<ActionEnnemis>().pointsDeVieActuels == ennemi.GetComponent<ActionEnnemis>().pointsDeVieMaximum);
        }
    }
}
