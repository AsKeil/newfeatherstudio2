﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class MenuOutils
{
#if UNITY_EDITOR
    
    //list des divers tuiles selon leur tag
    static GameObject[] foret;
    static GameObject[] terrain;
    static GameObject[] vierge;
    static GameObject[] eau;

    //Les divers materiels utilisé pour les tuiles.
    static Material viergeMaterial = Resources.Load<Material>("Materials/Vierge");
    static Material terrainMaterial = Resources.Load<Material>("Materials/Terrain");
    static Material eauMaterial = Resources.Load<Material>("Materials/Eau");
    static Material foretMaterial = Resources.Load<Material>("Materials/Foret");

    /// <summary>
    /// Fonction créant un nouvel onglet dans le menu outil.
    /// Fonction permettant d'assigner le materiel des tuiles.
    /// </summary>
    [MenuItem("Outils/Assigner Materiel Tuile")]
    public static void AssigneCaseMaterial()
    {

        //boucle qui verifie chaque case qui on le tag Foret, Eau ou Terrain
        foret = CreeListeTuilleForet();
        foreach (GameObject f in foret)
        {
            f.GetComponent<Renderer>().material = foretMaterial;
            f.GetComponent<Tuile>().estMarchable = true;
        }
        eau = CreeListeTuilleEau();
        foreach (GameObject e in eau)
        {
            e.GetComponent<Renderer>().material = eauMaterial;
            e.GetComponent<Tuile>().estMarchable = false;
        }
        terrain = CreeListeTuilleTerrain();
        foreach (GameObject t in terrain)
        {
            t.GetComponent<Renderer>().material = terrainMaterial;
            t.GetComponent<Tuile>().estMarchable = true;
        }
        vierge = CreeListeTuilleVierge();
        foreach (GameObject v in vierge)
        {
            v.GetComponent<Renderer>().material = viergeMaterial;
            v.GetComponent<Tuile>().estMarchable = true;
        }
    }

    /// <summary>
    /// Fonction créant un nouvel onglet dans le menu outil.
    /// Cette fonction assigne automatiquement le script "Tuile" aux tuiles.
    /// </summary>
    [MenuItem("Outils/Assigner Script Tuile")]
    public static void AssignerScript()
    {
        terrain = CreeListeTuilleTerrain();
        foreach (GameObject t in terrain)
        {
            VerifierScript(t);
        }
        eau = CreeListeTuilleEau();
        foreach (GameObject t in eau)
        {
            VerifierScript(t);
            t.GetComponent<Tuile>().estMarchable = false;
        }
        foret = CreeListeTuilleForet();
        foreach (GameObject t in foret)
        {
            VerifierScript(t);
        }
        vierge = CreeListeTuilleVierge();
        foreach (GameObject t in vierge)
        {
            VerifierScript(t);
        }
    }
    public static void VerifierScript(GameObject tuille)
    {
        if (!tuille.TryGetComponent<Tuile>(out Tuile t))
        {
            tuille.AddComponent<Tuile>();
        }
    }
    public static GameObject[] CreeListeTuilleForet()
    {
        return GameObject.FindGameObjectsWithTag("Tuile,Foret");        
    }
    public static GameObject[] CreeListeTuilleEau()
    {
        return GameObject.FindGameObjectsWithTag("Tuile,Eau");
    }
    public static GameObject[] CreeListeTuilleTerrain()
    {
        return GameObject.FindGameObjectsWithTag("Tuile,Terrain");
    }
    public static GameObject[] CreeListeTuilleVierge()
    {
        return GameObject.FindGameObjectsWithTag("Tuile,Vierge");
    }
#endif
}

