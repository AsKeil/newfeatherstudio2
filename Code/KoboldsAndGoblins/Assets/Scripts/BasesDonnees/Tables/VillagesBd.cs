﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "Villages" dans la base de données "Partie".
    /// </summary>
    public class VillagesBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champRacesEId = "RaceEid";
        private const string _champCartesEid = "CartesEid";
        private const string _champNom = "nom";
        private const string _champNombreDeJours = "nombreDeJours";
        private const string _champNourriture = "nourriture";
        private const string _champBois = "bois";
        private const string _champPierre = "pierre";
        private const string _champOrPiece = "orPiece";
        private const string _champVillageoisTotal = "villageoisTotal";
        private const string _champVillageoisDisponible = "villageoisDisponible";
        private const string _clePrimaire = "id";
        private RacesEBd _racesEBd;

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD de partie.</param>
        public VillagesBd(string nomBdPartie, string nomBdEncyclopedie) : base(nomBdPartie, "Villages")
        {
            _racesEBd = new RacesEBd(nomBdEncyclopedie);
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champRacesEId + " INTEGER NOT NULL, "
                + _champCartesEid + " INTEGER, "
                + _champNom + " TEXT NOT NULL, "
                + _champNombreDeJours + " INTEGER NOT NULL, "
                + _champNourriture + " INTEGER NOT NULL, "
                + _champBois + " INTEGER NOT NULL, "
                + _champPierre + " INTEGER NOT NULL, "
                + _champOrPiece + " INTEGER NOT NULL, "
                + _champVillageoisTotal + " INTEGER NOT NULL, "
                + _champVillageoisDisponible + " INTEGER NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="village"> L'entité à ajouter dans la BD.</param>
        public void AddData(VillageEntity village)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " (" + _champRacesEId
                + ", " + _champCartesEid
                + ", " + _champNom
                + ", " + _champNombreDeJours
                + ", " + _champNourriture
                + ", " + _champBois
                + ", " + _champPierre
                + ", " + _champOrPiece
                + ", " + _champVillageoisTotal
                + ", " + _champVillageoisDisponible
                + ") "

                + "VALUES ( @race"
                + ", " + 0
                + ", @nom"
                + ", @nombreDeJours"
                + ", @nourriture"
                + ", @bois"
                + ", @pierre"
                + ", @or"
                + ", @villageoisTotal"
                + ", @villageoisDisponible"
                + ")";
            AddParameterWithValue(dbcmd, "@race", _racesEBd.GetIdFromEntity(village.race));
            AddParameterWithValue(dbcmd, "@nom", village.nom);
            AddParameterWithValue(dbcmd, "@nombreDeJours", village.nombreDeJours);
            AddParameterWithValue(dbcmd, "@nourriture", village.nourriture);
            AddParameterWithValue(dbcmd, "@bois", village.bois);
            AddParameterWithValue(dbcmd, "@pierre", village.pierre);
            AddParameterWithValue(dbcmd, "@or", village.or);
            AddParameterWithValue(dbcmd, "@villageoisTotal", village.villageoisTotal);
            AddParameterWithValue(dbcmd, "@villageoisDisponible", village.villageoisDisponible);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table
        /// </summary>
        public List<VillageEntity> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<VillageEntity> liste = VillageEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<VillageEntity> VillageEntityParser(IDataReader reader)
        {
            List<VillageEntity> liste = new List<VillageEntity>();
            while (reader.Read())
            {
                VillageEntity village = new VillageEntity(
                    _racesEBd.GetEntityFromId(reader[1].ToString()),
                    reader[2].ToString(),
                    reader[3].ToString(),
                    int.Parse(reader[4].ToString()),
                    int.Parse(reader[5].ToString()),
                    int.Parse(reader[6].ToString()),
                    int.Parse(reader[7].ToString()),
                    int.Parse(reader[8].ToString()),
                    int.Parse(reader[9].ToString()),
                    int.Parse(reader[10].ToString()));
                liste.Add(village);
            }
            return liste;
        }

        /// <summary>
        /// Ferme toutes les connexions.
        /// </summary>
        public new void Close()
        {
            _racesEBd.Close();
            bdConnexion.Close();
            bdConnexion.Dispose();
        }
    }
}
