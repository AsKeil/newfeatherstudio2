﻿using System.Collections.Generic;
using Models;

namespace DataBank
{
    /// <summary>
    /// Permet de populer la base de donnée de l'encyclopédie. (ensemencement)
    /// </summary>
    public class PopulerEncyclopedie
    {
        /// <summary>
        /// Popule l'encyclopédie.
        /// </summary>
        public PopulerEncyclopedie()
        {
            PopulerRacesE();
            PopulerEquipementsE();
            PopulerBatimentsE();
            PopulerEvenementsE();
        }

        /// <summary>
        /// Popule la table EvenementsE.
        /// </summary>
        private void PopulerEvenementsE()
        {
            EvenementsEBd evenementsEBd = new EvenementsEBd(ApplicationData.NomBdEncyclopedie);
            if (evenementsEBd.GetNumOfRows() == 0)
            {
                List<EvenementEntity> listeEvenements = new List<EvenementEntity>();
                listeEvenements.Add(new EvenementEntity("Incendie",
                    "Un incendie ravage la réserve de bois.",
                    "Un incendie n'est un drame que s'il y a un mort. Dans ce cas-ci, c'est un bête accident."));
                listeEvenements.Add(new EvenementEntity("Le village est attaqué",
                    "Un groupe hostile se rapproche du village, il faut se défendre.",
                    "Pour survivre à demain, il faut protéger notre foyer coûte que coûte."));
                foreach (EvenementEntity evenement in listeEvenements)
                {
                    evenementsEBd.AddData(evenement);
                }
            }
            evenementsEBd.Close();
        }

        /// <summary>
        /// Popule la table RacesE.
        /// </summary>
        private void PopulerRacesE()
        {
            RacesEBd racesEBd = new RacesEBd(ApplicationData.NomBdEncyclopedie);
            if (racesEBd.GetNumOfRows() == 0)
            {
                List<RaceEntity> listeRaces = new List<RaceEntity>();
                listeRaces.Add(new RaceEntity("Kobold"));
                listeRaces.Add(new RaceEntity("Goblin"));
                listeRaces.Add(new RaceEntity("Orc"));
                listeRaces.Add(new RaceEntity("Humain"));
                listeRaces.Add(new RaceEntity("Nain"));
                listeRaces.Add(new RaceEntity("Elf"));
                foreach (RaceEntity race in listeRaces)
                {
                    racesEBd.AddData(race);
                }
            }
            racesEBd.Close();
        }

        /// <summary>
        /// Popule la table EquipementsE.
        /// </summary>
        private void PopulerEquipementsE()
        {
            EquipementsEBd equipementsEBd = new EquipementsEBd(ApplicationData.NomBdEncyclopedie);
            if (equipementsEBd.GetNumOfRows() == 0)
            {
                List<EquipementsEntity> listeEquipements = new List<EquipementsEntity>();
                listeEquipements.Add(new EquipementsEntity("arme", "Dague", 1, 0));
                listeEquipements.Add(new EquipementsEntity("arme", "Épée courte", 2, 0));
                listeEquipements.Add(new EquipementsEntity("arme", "Épée longue", 3, 0));
                listeEquipements.Add(new EquipementsEntity("armure", "Armure de cuir", 0, 1));
                listeEquipements.Add(new EquipementsEntity("armure", "Cotte de mailles", 0, 2));
                listeEquipements.Add(new EquipementsEntity("armure", "Armure de plates", 0, 3));
                foreach (EquipementsEntity equipement in listeEquipements)
                {
                    equipementsEBd.AddData(equipement);
                }
            }
            equipementsEBd.Close();
        }

        /// <summary>
        /// Popule la table BatimentsE.
        /// </summary>
        private void PopulerBatimentsE()
        {
            BatimentsEBd batimentsEBd = new BatimentsEBd(ApplicationData.NomBdEncyclopedie);
            if (batimentsEBd.GetNumOfRows() == 0)
            {
                List<BatimentEntity> listeBatiments = new List<BatimentEntity>();
                listeBatiments.Add(new BatimentEntity(
                    "Hutte",        // Nom
                    "Une hutte en bois pour abriter des villageois.", // Description
                    "Image URL",    // URL de l'image
                    0,              // Coût en nourriture
                    25,             // Coût en bois
                    0,              // Coût en pierre
                    0,              // Coût en or
                    0,              // Nombre de villageois maximum
                    10,             // Nombre de points de vie maximum
                    0));            // Nombre de points de défense
                listeBatiments.Add(new BatimentEntity(
                    "Forge",        // Nom
                    "Une forge permet de fabriquer des armes et armure en métal.", // Description
                    "Image URL",    // URL de l'image
                    0,              // Coût en nourriture
                    10,             // Coût en bois
                    50,             // Coût en pierre
                    10,             // Coût en or
                    2,              // Nombre de villageois maximum
                    25,             // Nombre de points de vie maximum
                    2));            // Nombre de points de défense
                listeBatiments.Add(new BatimentEntity(
                    "Marché",       // Nom
                    "Le marché permet aux villageois de faire des échanges et de produire de l'or.", // Description
                    "Image URL",    // URL de l'image
                    25,             // Coût en nourriture
                    25,             // Coût en bois
                    25,             // Coût en pierre
                    25,             // Coût en or
                    2,              // Nombre de villageois maximum
                    20,             // Nombre de points de vie maximum
                    1));            // Nombre de points de défense
                foreach (BatimentEntity batiment in listeBatiments)
                {
                    batimentsEBd.AddData(batiment);
                }
            }
            batimentsEBd.Close();
        }
    }
}