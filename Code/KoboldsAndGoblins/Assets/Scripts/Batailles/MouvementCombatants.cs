﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

/// <summary>
/// Class gérant les mouvements d'un combattant.
/// </summary>
public class MouvementCombatants : MonoBehaviour
{
    /// <summary>
    /// Liste de tuiles servant à montrer où le joueur peut se déplacé.
    /// La liste est remise à zéro une fois le tour du joueur terminé, 
    /// car celui-ci ne peut bouger qu'une fois par tour.
    /// </summary>
    public List<Tuile> tuileSelectionable = new List<Tuile>();

    //Liste de tuiles.
    Tuile[] tuiles;

    //Pile de tuiles représentant le chemin que le combattant ennemis doit effectuer.
    Stack<Tuile> chemin = new Stack<Tuile>();

    //Tuile sur laquelle le combattant se trouve au début de son tour.
    Tuile tuileCourante;

    //true si le combattant est en mouvement.
    public bool enMouvement = false;

    //true si le combattant a terminé de bouger.
    public bool finMouvement = false;

    //Vitesse de déplacement des combattants.
    float vitesse = 2;   
    
    Vector3 velocity = new Vector3();

    //Direction dans laquelle le joueur se dirige.
    Vector3 direction = new Vector3();
    
    float demisHauteur;

    // Tuile cible véritable de l'ennemi soit la tuile adjacente au combattant du joueur.
    public Tuile veritableTuileCible;

    /// <summary>
    /// initialise la liste des tuiles de la grille de combat, les listes de combattants et la demisHauteur.
    /// </summary>
    protected void Init()
    {
        tuiles = GameObject.FindObjectsOfType<Tuile>();


        demisHauteur = GetComponent<Collider>().bounds.extents.y;
    }
    /// <summary>
    /// Fonction trouvant la tuile courante des combattants actifs.
    /// </summary>
    public void SetTuileCourrante()
    {
        tuileCourante = TrouverTuileCible(gameObject);
    }
    public Tuile GetTuilecourante()
    {
        return tuileCourante;
    }
    /// <summary>
    /// Fonction enregistrant la tuile courante des combattants.
    /// </summary>
    /// <param name="cible">La tuile qui est ciblée pour le déplacement</param>
    /// <returns>Renvoie la tuile sur laquel le joueur se trouve</returns>
    public Tuile TrouverTuileCible(GameObject cible)
    {
        RaycastHit hit;
        Tuile tuile = null;

        if(Physics.Raycast(cible.transform.position, -Vector3.up, out hit,1))
        {
            tuile = hit.collider.GetComponent<Tuile>();     
        }
        return tuile;
        
    }
    /// <summary>
    /// Fonction créant les listes de tuiles adjacentes pour chaque 
    /// tuile qui est sélectionnable.
    /// </summary>
    public void CalculerListAdjancent(Tuile cibleTuile)
    {
        foreach(Tuile tuile in tuiles)
        {
            Tuile t = tuile.gameObject.GetComponent<Tuile>();
            t.TrouverVoisin(cibleTuile);

        }
    }
    /// <summary>
    /// Fonction servent à trouver tous les tuiles sélectionnables.
    /// En d’autres mots, la fonction permet de trouver toutes les tuiles
    /// sur lesquelles le joueur peut se déplacer.
    /// </summary>
    public void TrouverTuileSelectionable()
    {
        CalculerListAdjancent(null);       

        Queue<Tuile> process = new Queue<Tuile>();

        process.Enqueue(tuileCourante);
        tuileCourante.visiter = true;


        while (process.Count > 0)
        {
            Tuile t = process.Dequeue();
            
            tuileSelectionable.Add(t);
 
            t.estSelectionable = true;

            if (t.distance < GetComponent<ActionCombatants>().porteDeDeplacement)
            {                           
                foreach(Tuile tuile in t.adjacenteList)
                {
                    if (!tuile.visiter)
                    {
                        tuile.parent = t;
                        tuile.visiter = true;
                        tuile.distance = 1 + t.distance;
                        process.Enqueue(tuile);
                    }
                }
            }          
        }
        tuileCourante.positionActuel = true;
    }

    /// <summary>
    /// Fonction qui permet au joueur de bouger vers la tuile 
    /// cible(tuile sélectionné par le joueur).
    /// La fonction crée une pile(stack) en débutant par 
    /// la tuile cible jusqu'à la position du joueur.
    /// </summary>
    public void AtteindreDestination(Tuile tuile)
    {
        chemin.Clear();
        tuile.cible = true;
        enMouvement = true;

        Tuile suivante = tuile;
        while(suivante != null)
        {
            chemin.Push(suivante);
            suivante = suivante.parent;
        }
    }

    /// <summary>
    ///  Fonction faisant bouger les combattants.
    /// </summary>
    public void DeplacementCombattant()
    {
        if (chemin.Count > 0)
        {
            Tuile t = chemin.Peek();
            Vector3 cible = t.transform.position;

            //Calcule la position de l'unité sur le dessus de la tuile.
            cible.y += demisHauteur + t.GetComponent<Collider>().bounds.extents.y;

            if (Vector3.Distance(transform.position, cible) >= 0.05f)
            {
                CalculerDirection(cible);
                SetvitesseDeplacementHorizontal();

                transform.forward = direction;
                transform.position += velocity * Time.deltaTime;
            }
            else
            {
                //Le centre de la tuile est atteint.
                transform.position = cible;
                chemin.Pop();

            }
        }
        else
        {
            EnleverTuileSelectione();
            enMouvement = false;
            finMouvement = true;            
        }
    }

    /// <summary>
    /// Fonction permettant de vider la liste de tuiles sélectionnées.
    /// </summary>
    public void EnleverTuileSelectione()
    {
        if(tuileCourante != null)
        {
            tuileCourante.positionActuel = false;           
        }
        foreach (Tuile tuille in tuileSelectionable)
        {
            tuille.RetourArriere();
        }
        tuileSelectionable.Clear();
    }

    /// <summary>
    ///  Fonction permettant de trouver la direction à faire face.
    /// </summary>
    /// <param name="cible"> comabtant Npc </param>
    public void CalculerDirection(Vector3 cible)
    {
        direction = cible - transform.position;
        direction.Normalize();
    }

    /// <summary>
    /// Fonction définissant la vitesse de déplacement.
    /// </summary>
    public void SetvitesseDeplacementHorizontal()
    {
        velocity = direction * vitesse;
    }

    /// <summary>
    /// Fonction permetant de trouvé la tuile ayant le plus bas f.
    /// </summary>
    /// <param name="list">Liste de tuiles </param>
    /// <returns>Retourne la tuile ayant le plus bas f </returns>
    protected Tuile TrouverLePlusBasF(List<Tuile> list)
    {
        Tuile plusBas = list[0];
        foreach(Tuile t in list)
        {
            if (t.f < plusBas.f)
            {
                plusBas = t;
            }
        }

        list.Remove(plusBas);
        return plusBas;
    }
    /// <summary>
    /// Recherche la tuile où l'ennemi doit se rendre à la fin de son déplacement.
    /// </summary>
    /// <param name="t"> Tuile représentant la fin du déplacement</param>
    /// <returns> retourne la tuile de fin du mouvement</returns>
    protected Tuile TrouverFinTuile(Tuile t)
    {
        Stack<Tuile> tempPath = new Stack<Tuile>();

        Tuile suivante = t.parent;

        while(suivante != null)
        {
            tempPath.Push(suivante);
            suivante = suivante.parent;
        }
        if (tempPath.Count <= GetComponent<ActionCombatants>().porteDeDeplacement)
        {
            return t.parent;
        }

        Tuile finTuile = null;
        for(int i = 0; i <= GetComponent<ActionCombatants>().porteDeDeplacement; i++)
        {
            finTuile = tempPath.Pop();
        }
        return finTuile;
    }
    /// <summary>
    /// Fonction servant a trouver le chemin le plus rapide pour l'ennemi
    /// compare s’il y a plusieurs chemins et prend le plus rapide.
    /// </summary>
    /// <param name="cible"> Le combattant que l'ennemi cherche à atteindre.</param>
    protected void TrouverChemin( GameObject cible)
    {
        
        Tuile tuilleCouranteCibleTemp = cible.GetComponent<MouvementAllies>().GetTuilecourante();
        CalculerListAdjancent(tuilleCouranteCibleTemp);

        Tuile tuilleCouranteTemp = GetTuilecourante();

        //Toute les tuile qui non pas été calculer.
        List<Tuile> ouvetList = new List<Tuile>();

        // Toutes les tuiles qui ont été calculer.
        List<Tuile> fermertureList = new List<Tuile>();

        ouvetList.Add(tuilleCouranteTemp);

        tuilleCouranteTemp.h = Vector3.Distance(tuilleCouranteTemp.transform.position, tuilleCouranteCibleTemp.transform.position);
        tuilleCouranteTemp.f = tuilleCouranteTemp.h;
        while(ouvetList.Count > 0)
        {
            Tuile t = TrouverLePlusBasF(ouvetList);

            fermertureList.Add(t);

            if ( t == tuilleCouranteCibleTemp)
            {
                veritableTuileCible = TrouverFinTuile(t);
                AtteindreDestination(veritableTuileCible);
                return;
            }
            foreach( Tuile tuile in t.adjacenteList)
            {
                //Si la tuile a déjà été ajoutée à la fermeture de la liste.
                if (fermertureList.Contains(tuile))
                {
                    //fait rien 
                }
                //S’il y a un deuxième chemin possible, voir s'il est plus rapide.
                else if (ouvetList.Contains(tuile))
                {
                    float tempG = t.g + Vector3.Distance(tuile.transform.position, t.transform.position);

                    if ( tempG < tuile.g) 
                    {
                        tuile.parent = t;

                        tuile.g = tempG;
                        tuile.f = tuile.g + tuile.h;
                    }
                }
                else
                {
                    tuile.parent = t;

                    tuile.g = t.g + Vector3.Distance(tuile.transform.position, t.transform.position);
                    tuile.h = Vector3.Distance(tuile.transform.position, tuilleCouranteCibleTemp.transform.position);
                    tuile.f = tuile.h + tuile.g;

                    ouvetList.Add(tuile);
                }
            }
        }
        //todo - que fair lorsqu'il n'y a pas de chemin pour ce rendre a la cible
        UnityEngine.Debug.Log("Chemin non trouver");
        
    }
    
     
}