﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarDeVie : MonoBehaviour
{
    
    public string ptVieMax = "";
    public string affTxt = "";
    public void SetPtVieTxt(int vie)
    {
        
        ptVieMax = vie.ToString();
        affTxt = vie + " / " + ptVieMax;
        GetComponentInParent<Text>().text = affTxt;
    }
    public void ChangementPtVie(int vieActuel)
    {
        affTxt = vieActuel + " / " + ptVieMax;
        GetComponentInParent<Text>().text = affTxt;
    }


}
