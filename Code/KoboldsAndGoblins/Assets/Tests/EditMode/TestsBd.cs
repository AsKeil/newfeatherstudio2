﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Tests
{
    /// <summary>
    /// Classe abstraite pour faciliter les tests de BD.
    /// </summary>
    public abstract class TestsBd
    {
        public readonly string cheminBd = Application.persistentDataPath + "/";
        private readonly List<string> ApplicationDataValeurDefaut = new List<string>(new string[] {
            ApplicationData.NomBdEncyclopedie, ApplicationData.NomPartie, ApplicationData.NomBdSauvegarde });
        private readonly List<string> ApplicationDataValeurTest = new List<string>(new string[] {
           "encyclopedieTest.db3", "partieTest", "sauvegardeTest.db3" });

        /// <summary>
        /// Supprime les bases de données de test si elles existent.
        /// Met applicationData en mode régulier.
        /// </summary>
        public void SupprimerBdTest()
        {
            SetApplicationDataTest(true);

            // Fait un appel pour se débarasser des éléments pending dans le "Garbage Collector" avant de faire la supression.
            // Permet d'éviter certains bugs.
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();

            if (File.Exists(cheminBd + ApplicationData.NomBdEncyclopedie))
            {
                File.Delete(cheminBd + ApplicationData.NomBdEncyclopedie);
            }
            foreach (string partie in Directory.EnumerateFiles(cheminBd, (ApplicationData.NomPartie+"*.db3")))
            {
                File.Delete(partie);
            }
            if (File.Exists(cheminBd + ApplicationData.NomBdSauvegarde))
            {
                File.Delete(cheminBd + ApplicationData.NomBdSauvegarde);
            }
        }

        /// <summary>
        /// Met en place ApplicationData pour travailler avec des données de test.
        /// </summary>
        /// <param name="modeTest"> false pour les valeurs par défaut, true pour pour les valeurs de test.</param>
        public void SetApplicationDataTest(Boolean modeTest)
        {
            if (modeTest == false)
            {
                ApplicationData.NomBdEncyclopedie = ApplicationDataValeurDefaut[0];
                ApplicationData.NomPartie = ApplicationDataValeurDefaut[1];
                ApplicationData.NomBdSauvegarde = ApplicationDataValeurDefaut[2];
            } else if (modeTest == true)
            {
                ApplicationData.NomBdEncyclopedie = ApplicationDataValeurTest[0];
                ApplicationData.NomPartie = ApplicationDataValeurTest[1];
                ApplicationData.NomBdSauvegarde = ApplicationDataValeurTest[2];
            }
        }
    }
}
