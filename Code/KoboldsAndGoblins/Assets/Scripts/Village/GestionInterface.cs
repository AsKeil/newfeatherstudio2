﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GestionInterface : MonoBehaviour
{
    public GameObject prefabBatiment;
    public GameObject prefabCombattant;
    public GameObject prefabEvenement;
    public Transform Batiments;
    public Transform Combattants;
    public Transform Evenements;
    public GameObject DetailCombattant;
    public GameObject DetailEvenement;
    public GameObject Ressources;
    public GameObject MenuConstruction;
    protected GameObject GameManager;
    

    protected virtual void Start()
    {
        GameManager = GameObject.Find("GameManager");
        ChargerBatiment();
        ChargerCombattants();
        ChargerEvenements();
        DetailCombattant.SetActive(false);
        DetailEvenement.SetActive(false);
        MenuConstruction.SetActive(false);
    }

    /// <summary>
    /// Mets à jour les valeurs des ressources à chaque image.
    /// </summary>
    protected virtual void Update()
    {
        MiseAJourRessources();
    }

    /// <summary>
    /// Charge les bâtiments à partir d'ApplicationData et 
    /// affiche les combattants du village dans l'interface utilisateur.
    /// </summary>
    protected void ChargerBatiment()
    {
        if (ApplicationData.ListeBatimentsE != null)
        {
            foreach (Models.BatimentEntity batiment in ApplicationData.ListeBatimentsE)
            {
                CreerUIBatiment(batiment);
            }
        }
    }

    /// <summary>
    /// Créer l'interface utilisateur d'un bâtiment.
    /// </summary>
    /// <param name="batiment"> Le bâtiment dont il faut créer le IU </param>
    protected void CreerUIBatiment(Models.BatimentEntity batiment)
    {
        // Création du IU Batiment
        GameObject b = GameObject.Instantiate(prefabBatiment, Batiments);
        // Assigner les variables de l'objet
        b.GetComponent<BatimentVariable>().batiment = batiment;
        // Affiche les informations relatives au Batiment
        b.transform.Find("Nom").GetComponent<TextMeshProUGUI>().SetText(batiment.nom);
        b.transform.Find("Description").GetComponent<TextMeshProUGUI>().SetText(batiment.description);
        b.transform.Find("Villageois").GetComponent<TextMeshProUGUI>().SetText(batiment.nombreDevillageoisMaximum.ToString());
        b.transform.Find("Cout").GetComponent<TextMeshProUGUI>().SetText("Nourriture(" + batiment.coutNourriture.ToString() + "), Bois(" +
                batiment.coutBois.ToString() + "), Pierre(" + batiment.coutPierre.ToString() + "), Or(" + batiment.coutOr.ToString() + ")");
        // Fait la gestion du bouton Construire
        b.transform.Find("Bouton-Construire").GetComponent<Button>().onClick.AddListener(delegate
        {
            if (GameManager.GetComponent<GererRessources>().VerifierCoutRessources(
                b.GetComponent<BatimentVariable>().batiment.coutNourriture,
                b.GetComponent<BatimentVariable>().batiment.coutBois,
                b.GetComponent<BatimentVariable>().batiment.coutPierre,
                b.GetComponent<BatimentVariable>().batiment.coutOr))
            {
                GameManager.GetComponent<PlacerBatiment>().ActiverConstruction(true, b.GetComponent<BatimentVariable>().batiment);
                GameManager.GetComponent<GestionInterface>().GererAffichageMenuConstruction();
            }
            GameManager.GetComponent<GererRessources>().EnleverRessource(
                b.GetComponent<BatimentVariable>().batiment.coutNourriture,
                b.GetComponent<BatimentVariable>().batiment.coutBois,
                b.GetComponent<BatimentVariable>().batiment.coutPierre,
                b.GetComponent<BatimentVariable>().batiment.coutOr);
        });
    }

    /// <summary>
    /// REQ-8-4 Afficher les combattants.
    /// Charge les combattants à partir d'ApplicationData et
    /// affiche les combattants du village dans l'interface utilisateur.
    /// </summary>
    protected void ChargerCombattants()
    {
        if (ApplicationData.listeCombattants != null)
        {
            foreach (Models.CombattantsEntity combattant in ApplicationData.listeCombattants)
            {
                CreerUICombattant(combattant);
            }
        }
    }

    /// <summary>
    /// Créer l'interface utilisateur d'un combattant.
    /// </summary>
    /// <param name="combattant">Le combattant à afficher.</param>
    protected void CreerUICombattant(Models.CombattantsEntity combattant)
    {
        // Création du UI Combattant
        GameObject c = GameObject.Instantiate(prefabCombattant, Combattants);
        // Assigner les variables de l'objet
        c.GetComponent<CombattantVariable>().combattant = combattant;
        // Affiche les informations relatives au Combattant
        c.transform.Find("Nom").GetComponent<TextMeshProUGUI>().SetText(combattant.nom);
        c.transform.Find("Race").GetComponent<TextMeshProUGUI>().SetText(combattant.race.nomRace);
        c.transform.Find("Valeur-Attaque").GetComponent<TextMeshProUGUI>().SetText(combattant.pointsDAttaque.ToString());
        c.transform.Find("Valeur-Defense").GetComponent<TextMeshProUGUI>().SetText(combattant.pointsDeDefense.ToString());
        // Fait la gestion du bouton Combattant
        c.GetComponent<Button>().onClick.AddListener(delegate
        {
            GameManager.GetComponent<GestionInterface>().GererAffichageDetailCombattant(c.GetComponent<CombattantVariable>().combattant);
        });
    }

    /// <summary>
    /// REQ-8-10 Inspecter les combattants.
    /// Active ou désactive DetailCombattant selon l'état de celui-ci.
    /// Mis-à-jour selon le combattant visé.
    /// </summary>
    /// <param name="combattant">Le combattant à afficher.</param>
    public void GererAffichageDetailCombattant(Models.CombattantsEntity combattant)
    {
        ModifierDonneeDetailCombattant(combattant);
        DetailCombattant.SetActive(!DetailCombattant.activeSelf);
    }

    /// <summary>
    /// Modifie les données de DetailCombattant pour afficher les données du combattant
    /// </summary>
    /// <param name="combattant">Le combattant à afficher.</param>
    protected void ModifierDonneeDetailCombattant(Models.CombattantsEntity combattant)
    {
        DetailCombattant.transform.Find("Nom").GetComponent<TextMeshProUGUI>().SetText(combattant.nom);
        DetailCombattant.transform.Find("Race").GetComponent<TextMeshProUGUI>().SetText(combattant.race.nomRace);
        DetailCombattant.transform.Find("PointsdeVie").GetComponent<TextMeshProUGUI>().SetText(combattant.pointsDeVieMaximum.ToString());
        DetailCombattant.transform.Find("Attaque").GetComponent<TextMeshProUGUI>().SetText(combattant.pointsDAttaque.ToString());
        DetailCombattant.transform.Find("Defense").GetComponent<TextMeshProUGUI>().SetText(combattant.pointsDeDefense.ToString());
        DetailCombattant.transform.Find("Vision").GetComponent<TextMeshProUGUI>().SetText(combattant.porteeVision.ToString());
        DetailCombattant.transform.Find("Deplacement").GetComponent<TextMeshProUGUI>().SetText(combattant.porteeDeplacement.ToString());
        if (combattant.arme != null)
        {
            DetailCombattant.transform.Find("Arme").GetComponent<TextMeshProUGUI>().SetText(combattant.arme.nom);
        }
        else
        {
            DetailCombattant.transform.Find("Arme").GetComponent<TextMeshProUGUI>().SetText("Aucune");
        }
        if (combattant.armure != null)
        {
            DetailCombattant.transform.Find("Armure").GetComponent<TextMeshProUGUI>().SetText(combattant.armure.nom);
        }
        else
        {
            DetailCombattant.transform.Find("Armure").GetComponent<TextMeshProUGUI>().SetText("Aucune");
        }
    }

    /// <summary>
    /// REQ-8-5 Afficher les événements.
    /// Active ou désactive DetailEvenement selon l'état de celui-ci.
    /// Mis-à-jour selon l'événement visé.
    /// </summary>
    /// <param name="evenement">L'événement à afficher.</param>
    public void GererAffichageDetailEvenement(Models.EvenementEntity evenement)
    {
        // Modifie les données affichées de DetailEvenement
        DetailEvenement.transform.Find("Nom").GetComponent<TextMeshProUGUI>().SetText(evenement.nom);
        DetailEvenement.transform.Find("Description").GetComponent<TextMeshProUGUI>().SetText(evenement.description);
        DetailEvenement.transform.Find("TexteAgrement").GetComponent<TextMeshProUGUI>().SetText(evenement.texteAgrement);
        // Modifie l'état de DetailEvenement
        DetailEvenement.SetActive(!DetailEvenement.activeSelf);
    }

    /// <summary>
    /// REQ-8-5 Afficher les événements.
    /// Charge les événements à partir d'ApplicationData et 
    /// affiche les événements du village dans l'interface utilisateur.
    /// </summary>
    public void ChargerEvenements()
    {
        if (ApplicationData.listeEvenements != null)
        {
            foreach (Models.EvenementEntity evenement in ApplicationData.listeEvenements)
            {
                CreerUIEvenement(evenement);
            }
        }
    }

    /// <summary>
    /// Crée l'interface utilisateur d'un événement.
    /// </summary>
    /// <param name="evenement">L'événement à afficher.</param>
    protected void CreerUIEvenement(Models.EvenementEntity evenement)
    {
        // Création du UI Evenement
        GameObject e = GameObject.Instantiate(prefabEvenement, Evenements);
        // Assigner les variables de l'objet
        e.GetComponent<EvenementVariable>().evenement = evenement;
        // Affiche les informations relatives a l'evenement
        e.transform.Find("NbJour").GetComponent<TextMeshProUGUI>().SetText(evenement.chrono.ToString());
        e.transform.Find("Description").GetComponent<TextMeshProUGUI>().SetText(evenement.description);
        // Fait la gestion du bouton Evenement
        e.GetComponent<Button>().onClick.AddListener(delegate
        {
            GameManager.GetComponent<GestionInterface>().GererAffichageDetailEvenement(e.GetComponent<EvenementVariable>().evenement);
        });
    }

    /// <summary>
    /// REQ-8-1 Afficher les ressources disponibles.
    /// </summary>
    public void MiseAJourRessources()
    {
        if (ApplicationData.village != null)
        {
            Ressources.transform.Find("Nourriture-Inv").GetComponent<TextMeshProUGUI>().SetText(ApplicationData.village.nourriture.ToString());
            Ressources.transform.Find("Bois-Inv").GetComponent<TextMeshProUGUI>().SetText(ApplicationData.village.bois.ToString());
            Ressources.transform.Find("Pierre-Inv").GetComponent<TextMeshProUGUI>().SetText(ApplicationData.village.pierre.ToString());
            Ressources.transform.Find("Or-Inv").GetComponent<TextMeshProUGUI>().SetText(ApplicationData.village.or.ToString());
            Ressources.transform.Find("VillageoisTotaux").GetComponent<TextMeshProUGUI>().SetText(ApplicationData.village.villageoisTotal.ToString());
            Ressources.transform.Find("VillageoisDisponible").GetComponent<TextMeshProUGUI>().SetText(ApplicationData.village.villageoisDisponible.ToString());
        }
    }

    /// <summary>
    /// Active ou désactive MenuConstruction selon l'état de celui-ci.
    /// </summary>
    public void GererAffichageMenuConstruction()
    {
        MenuConstruction.SetActive(!MenuConstruction.activeSelf);
    }
}
