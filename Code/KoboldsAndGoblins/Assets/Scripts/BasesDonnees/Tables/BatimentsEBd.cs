﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "BatimentsE" dans la base de données "Encyclopedie"
    /// </summary>
    public class BatimentsEBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champNom = "nom";
        private const string _description = "description";
        private const string _image = "image";
        private const string _coutNourriture = "coutNourriture";
        private const string _coutBois = "coutBois";
        private const string _coutPierre = "coutPierre";
        private const string _coutOr = "coutOr";
        private const string _nombreDeVillageoisMaximum = "nombreDeVillageoisMaximum";
        private const string _pointsDeVieMaximum = "pointsDeVieMaximum";
        private const string _pointsDeDefense = "pointsDeDefense";
        private const string _clePrimaire = "id";

        public string NomTable { get { return nom_table; } }
        public string ChampId { get { return _champId; } }

        /// <summary>
        /// Crée la BD si elle n'existe pas.
        /// Ouvre la connexion.
        /// </summary>
        /// <param name="nomTable">Le nom la de la table de cette BD.</param>
        public BatimentsEBd(string nomBdEncyclopedie) : base(nomBdEncyclopedie, "BatimentsE")
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champNom + " TEXT NOT NULL, "
                + _description + " TEXT NOT NULL, "
                + _image + " TEXT NOT NULL, "
                + _coutNourriture + " INTEGER NOT NULL, "
                + _coutBois + " INTEGER NOT NULL, "
                + _coutPierre + " INTEGER NOT NULL, "
                + _coutOr + " INTEGER NOT NULL, "
                + _nombreDeVillageoisMaximum + " INTEGER NOT NULL, "
                + _pointsDeVieMaximum + " INTEGER NOT NULL, "
                + _pointsDeDefense + " INTEGER NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="batiment"> L'entité à ajouter dans la BD.</param>
        public void AddData(BatimentEntity batiment)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champNom + ", "
                + _description + ", "
                + _image + ", "
                + _coutNourriture + ", "
                + _coutBois + ", "
                + _coutPierre + ", "
                + _coutOr + ", "
                + _nombreDeVillageoisMaximum + ", "
                + _pointsDeVieMaximum + ", "
                + _pointsDeDefense
                + " ) "

                + "VALUES ( @nom"
                + ", @description"
                + ", @image"
                + ", @coutNourriture"
                + ", @coutBois"
                + ", @coutPierre"
                + ", @coutOr"
                + ", @nombreDevillageoisMaximum"
                + ", @pointsDeVieMaximum"
                + ", @pointsDeDefense"
                + " )";
            AddParameterWithValue(dbcmd, "@nom", batiment.nom);
            AddParameterWithValue(dbcmd, "@description", batiment.description);
            AddParameterWithValue(dbcmd, "@image", batiment.image);
            AddParameterWithValue(dbcmd, "@coutNourriture", batiment.coutNourriture);
            AddParameterWithValue(dbcmd, "@coutBois", batiment.coutBois);
            AddParameterWithValue(dbcmd, "@coutPierre", batiment.coutPierre);
            AddParameterWithValue(dbcmd, "@coutOr", batiment.coutOr);
            AddParameterWithValue(dbcmd, "@nombreDevillageoisMaximum", batiment.nombreDevillageoisMaximum);
            AddParameterWithValue(dbcmd, "@pointsDeVieMaximum", batiment.pointsDeVieMaximum);
            AddParameterWithValue(dbcmd, "@pointsDeDefense", batiment.pointsDeDefense);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<BatimentEntity> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<BatimentEntity> liste = BatimentsEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Retourne une entité à partir de son ID.
        /// </summary>
        /// <param name="id"> Le ID de l'entité cherchée.</param>
        /// <returns> L'entité cherchée.</returns>
        public BatimentEntity GetEntityFromId(string id)
        {
            IDataReader reader = GetDataFromString(_champId, id);
            List<BatimentEntity> liste = BatimentsEntityParser(reader);
            reader.Dispose();
            return liste[0];
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<BatimentEntity> BatimentsEntityParser(IDataReader reader)
        {
            List<BatimentEntity> liste = new List<BatimentEntity>();
            while (reader.Read())
            {
                BatimentEntity batiment = new BatimentEntity(
                    reader[1].ToString(),
                    reader[2].ToString(),
                    reader[3].ToString(),
                    int.Parse(reader[4].ToString()),
                    int.Parse(reader[5].ToString()),
                    int.Parse(reader[6].ToString()),
                    int.Parse(reader[7].ToString()),
                    int.Parse(reader[8].ToString()),
                    int.Parse(reader[9].ToString()),
                    int.Parse(reader[10].ToString()));
                liste.Add(batiment);
            }
            return liste;
        }

        /// <summary>
        /// Retourne un ID à partir d'une entité.
        /// </summary>
        /// <param name="batiment"> L'entité dont on cherche le ID.</param>
        internal string GetIdFromEntity(BatimentEntity batiment)
        {
            IDataReader reader = GetDataFromString(_champNom, batiment.nom);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }

        /// <summary>
        /// Retourne le ID d'une entitée à partir de son nom.
        /// </summary>
        /// <param name="nom"> Le nom de l'entité dont on cherche le ID.</param>
        /// <returns> Le ID de l'entitée.</returns>
        internal string GetIdFromNom(string nom)
        {
            IDataReader reader = GetDataFromString(_champNom, nom);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }
    }
}
