﻿

namespace Models
{
    /// <summary>
    /// DEF-12-12 Race
    /// Dans le jeu, une race est considérée comme une espèce que le joueur peut rencontrer ou incarner. 
    /// Exemple: elf, gobelin, humain, kobold, nain, orc, etc.
    /// </summary>
    public class RaceEntity
    {
        public string nomRace;

        /// <summary>
        /// Constructeur pour les races venant de l'encyclopédie.
        /// </summary>
        /// <param name="nomRace">Le nom de la race</param>
        public RaceEntity(string nomRace)
        {
            this.nomRace = nomRace;
        }
    }
}
