﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Variable qui spécifie quel type de bâtiment il est.
/// Associer au prefab Bâtiment.
/// </summary>
public class BatimentVariable : MonoBehaviour
{
    [HideInInspector]
    public Models.BatimentEntity batiment;
}
