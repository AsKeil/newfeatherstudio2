﻿using Models;
using NUnit.Framework;
using DataBank;
using System.Collections.Generic;

namespace Tests
{
    public class ApplicationDataTests : TestsBd
    {
        /// <summary>
        /// Valide que les données critiques de la partie ne sont pas nulles lors d'une nouvelle partie.
        /// </summary>
        [Test]
        public void ApplicationDataNouvellePartie()
        {
            // Mise en place
            SetApplicationDataTest(true);
            ApplicationData.PartieNull();
            ApplicationData.NouvellePartie(new RaceEntity("raceTest"), "nomVillagetest");

            // Vérification
            Assert.IsNotNull(ApplicationData.village);
            Assert.IsNotNull(ApplicationData.listeCombattants);
            Assert.IsNotNull(ApplicationData.listeBatimentsConstruits);
            Assert.IsNotNull(ApplicationData.listeEquipementsQuantites);
            Assert.IsNotNull(ApplicationData.listeEvenements);

            Assert.IsNotNull(ApplicationData.ListeSauvegardes);
            Assert.IsNotNull(ApplicationData.ListeRacesE);
            Assert.IsNotNull(ApplicationData.ListeBatimentsE);
            Assert.IsNotNull(ApplicationData.ListeEquipementsE("arme"));

            // Nettoyage
            ApplicationData.PartieNull();
            SetApplicationDataTest(false);
        }

        /// <summary>
        /// Valide que les données sauvegardées et chargées sont les mêmes.
        /// TODO: Il faudrait utiliser du 'DeepCompare' pour s'assurer que les attribut sont tous identiques.
        /// </summary>
        [Test]
        public void ApplicationDataSauvegarderCharger()
        {
            // Mise en place
            SetApplicationDataTest(true);
            new PopulerEncyclopedie();
            ApplicationData.NouvellePartie(ApplicationData.ListeRacesE[0], "nomVillagetest");
            
            VillageEntity village = ApplicationData.village;
            List<CombattantsEntity> listeCombattants = new List<CombattantsEntity>(ApplicationData.listeCombattants);
            List<BatimentEntity> listeBatimentsConstruits = new List<BatimentEntity>(ApplicationData.listeBatimentsConstruits);
            List<(EquipementsEntity, int)> listeEquipementsQuantites = new List<(EquipementsEntity, int)>(ApplicationData.listeEquipementsQuantites);
            List<EvenementEntity> listeEvenements = new List<EvenementEntity>(ApplicationData.listeEvenements);
            ApplicationData.SauvegarderPartie();
            ApplicationData.ChargerPartie(ApplicationData.ListeSauvegardes[0]);

            // Vérification
            Assert.AreEqual(village.nom, ApplicationData.village.nom);
            Assert.AreEqual(village.race.nomRace, ApplicationData.village.race.nomRace);
            Assert.AreEqual(listeCombattants.Count, ApplicationData.listeCombattants.Count);
            Assert.AreEqual(listeBatimentsConstruits.Count, ApplicationData.listeBatimentsConstruits.Count);
            Assert.AreEqual(listeEquipementsQuantites.Count, ApplicationData.listeEquipementsQuantites.Count);
            Assert.AreEqual(listeEvenements.Count, ApplicationData.listeEvenements.Count);

            // Nettoyage
            SupprimerBdTest();
            ApplicationData.PartieNull();
            SetApplicationDataTest(false);
        }
    }
}
