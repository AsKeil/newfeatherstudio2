﻿using Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "EvenementsE" dans la base de données "Encyclopedie".
    /// </summary>
    public class EvenementsEBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champNom = "nom";
        private const string _description = "description";
        private const string _texteAgrement = "texteAgrement";
        private const string _clePrimaire = "id";

        public string NomTable { get { return nom_table; } }
        public string ChampId { get { return _champId; } }
        public string Nom { get { return _champNom; } }

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD encyclopédie.</param>
        public EvenementsEBd(string nomBdEncyclopedie) : base(nomBdEncyclopedie, "EvenementsE")
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champNom + " TEXT NOT NULL, "
                + _description + " TEXT NOT NULL, "
                + _texteAgrement + " TEXT NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="evenement"> L'entité à ajouter dans la BD.</param>
        public void AddData(EvenementEntity evenement)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champNom + ", "
                + _description + ", "
                + _texteAgrement
                + " ) "

                + "VALUES ( "
                + "@nom"
                + ", @description"
                + ", @texteAgrement"
                + " )";
            AddParameterWithValue(dbcmd, "@nom", evenement.nom);
            AddParameterWithValue(dbcmd, "@description", evenement.description);
            AddParameterWithValue(dbcmd, "@texteAgrement", evenement.texteAgrement);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<EvenementEntity> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<EvenementEntity> liste = EvenementEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Retourne une entité à partir de son ID.
        /// </summary>
        /// <param name="id"> Le ID de l'entité cherchée.</param>
        /// <returns> L'entité cherchée.</returns>
        public EvenementEntity GetEntityFromId(string id)
        {
            IDataReader reader = GetDataFromString(_champId, id);
            List<EvenementEntity> liste = EvenementEntityParser(reader);
            reader.Dispose();
            return liste[0];
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<EvenementEntity> EvenementEntityParser(IDataReader reader)
        {
            List<EvenementEntity> liste = new List<EvenementEntity>();
            while (reader.Read())
            {
                EvenementEntity evenement = new EvenementEntity(
                    reader[1].ToString(),
                    reader[2].ToString(),
                    reader[3].ToString());
                liste.Add(evenement);
            }
            return liste;
        }

        /// <summary>
        /// Retourne un ID à partir d'une entité.
        /// </summary>
        internal string GetIdFromEntity(EvenementEntity evenement)
        {
            IDataReader reader = GetDataFromString(_champNom, evenement.nom);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }

        /// <summary>
        /// Retourne le ID d'une entitée à partir de son nom.
        /// </summary>
        /// <param name="nom"> Le nom de l'entité dont on cherche le ID.</param>
        /// <returns> Le ID de l'entitée.</returns>
        internal string GetIdFromNom(string nom)
        {
            IDataReader reader = GetDataFromString(_champNom, nom);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }
    }
}
