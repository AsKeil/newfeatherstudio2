﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class gérant les actions que les combattants allié seulement.
/// </summary>
public class ActionAllies : ActionCombatants
{
    //Booléen indiquant qu'un ennemi est trouvé si a true sinon false.
    bool ennemiTrouver =false;

    // Start is called before the first frame update
    void Start()
    {
        Init();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<MouvementAllies>().finMouvement)
        {
            PositionAdjacente();
            TrouverEnnemisAdjacent();
            if (ennemiTrouver)
            {
                VerifierSourisAttaque();
                if (GetComponent<ActionCombatants>().attaqueEffectuer)
                {                    
                    GestionaireTour.FinTour();
                }
            }
            else
            {
                GestionaireTour.FinTour();
            }
        }
        else
        {
            return;
        }
    }
    /// <summary>
    /// Trouve les ennemis sur les cases adjacentes mettant leur couleur de matériau à noire.
    /// </summary>
    public void TrouverEnnemisAdjacent()
    {      
        List<ActionCombatants> listEnnemis;
        listEnnemis =GestionaireTour.combatants["Ennemi"] ;
        foreach(ActionCombatants ennemi in listEnnemis)
        {
            foreach(Vector3 posAdjascente in listPositionAdjascente)
            {
                if (ennemi.transform.position == posAdjascente)
                {
                    ennemi.GetComponent<Renderer>().material.color = Color.black;
                    ennemiTrouver = true;
                }
            }            
        }
    }
    /// <summary>
    /// Verifie le clic de souris sur la cible et lance l'attaque.
    /// </summary>
    public void VerifierSourisAttaque()
    {       
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.GetComponent<ActionEnnemis>())
                {
                    ActionEnnemis CombatantEnnemi= hit.collider.GetComponent<ActionEnnemis>();
                    if (CombatantEnnemi.pointsDeVieActuels>0)
                    {
                        Attaquer(CombatantEnnemi,pointsDAttaque);
                    }
                }
            }
        }       
    }
    /// <summary>
    /// Attaque la cible.
    /// </summary>
    /// <param name="cible"> Combattant ennemi</param>
    public void Attaquer(ActionCombatants cible, int pointsDAttaque)
    {
        cible.GetComponent<ActionEnnemis>().DomageRecus(pointsDAttaque);
        
        attaqueEffectuer = true;
        ennemiTrouver = false;
        RetourMateriauxEnnemi();
    }
    /// <summary>
    /// Fonction qui remet le matériau original aux ennemis suite à l'attaque.
    /// </summary>
    public void RetourMateriauxEnnemi()
    {
        foreach (ActionCombatants ennemi in GestionaireTour.combatants["Ennemi"])
        {
            ennemi.GetComponent<Renderer>().material.color= Color.red ;
        }
    }
}
