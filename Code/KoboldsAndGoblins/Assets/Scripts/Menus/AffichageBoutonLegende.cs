﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe qui gère l'affichage de la legende.
/// </summary>
public class AffichageBoutonLegende : MonoBehaviour
{
    public GameObject affichable;

    public void Start()
    {
        affichable.SetActive(false);
    }

    /// <summary>
    /// Permet d'afficher et de désactiver l'affichage de la legende.
    /// </summary>
    public void GererAffichage()
    {
        if (affichable.activeSelf)
        {
            affichable.SetActive(false);
        }
        else
        {
            affichable.SetActive(true);
        }
    }
}
