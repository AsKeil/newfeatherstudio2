﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "Evenements" dans la base de données "Partie".
    /// </summary>
    public class EvenementsBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champEvenementsEid = "EvenementsEid";
        private const string _champChrono = "chrono";
        private const string _clePrimaire = "id";
        private EvenementsEBd _evenementsEBd;

        public string NomTable { get { return nom_table; } }
        public string ChampId { get { return _champId; } }

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdPartie"> Le nom de la BD de partie.</param>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD encyclopédie.</param>
        public EvenementsBd(string nomBdPartie, string nomBdEncyclopedie) : base(nomBdPartie, "Evenements")
        {
            _evenementsEBd = new EvenementsEBd(nomBdEncyclopedie);
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champEvenementsEid + " INTEGER NOT NULL, "
                + _champChrono + " INTEGER NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="evenement"> L'entité à ajouter dans la BD.</param>
        public void AddData(EvenementEntity evenement)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champEvenementsEid + ", "
                + _champChrono
                + " ) "

                + "VALUES ( @evenement"
                + ", @chrono"
                + " )";
            AddParameterWithValue(dbcmd, "@evenement", _evenementsEBd.GetIdFromEntity(evenement));
            AddParameterWithValue(dbcmd, "@chrono", evenement.chrono);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<EvenementEntity> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<EvenementEntity> liste = EvenementEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<EvenementEntity> EvenementEntityParser(IDataReader reader)
        {
            List<EvenementEntity> liste = new List<EvenementEntity>();

            while (reader.Read())
            {
                EvenementEntity evenement = _evenementsEBd.GetEntityFromId(reader[0].ToString());
                evenement.groupeEvenement = int.Parse(reader[0].ToString());
                evenement.chrono = int.Parse(reader[2].ToString());
                liste.Add(evenement);
            }
            return liste;
        }

        /// <summary>
        /// Ferme toutes les connexions.
        /// </summary>
        public new void Close()
        {
            _evenementsEBd.Close();
            bdConnexion.Close();
            bdConnexion.Dispose();
        }
    }
}
