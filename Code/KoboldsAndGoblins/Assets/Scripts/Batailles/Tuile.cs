﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuile : MonoBehaviour
{
    // current = Position actuelle du joueur.
    public bool positionActuel = false;

    //cible = Position où le joueur veut ce déplacé.
    public bool cible = false;

    // estSelectionable vérifie que la tuile est accessible.
    public bool estSelectionable = false;

    // La variable estMarchable permet de dire si la tuile est marchable ou non.
    public bool estMarchable = true;

    //Liste de tuile adjacente à la position actuelle du joueur.
    public List<Tuile> adjacenteList = new List<Tuile>();

    //Besoin pour un système BFS(breadt first search)
    //la variable visiter et parent sont des drapeaux pour se retrouver
    //dans la grille du jeu
    public bool visiter = false;
    public Tuile parent = null;
    public int distance = 0;

    //variable pour A* 
    //la variable f represente la distance la plus courte pour atteindre la destination.
    public float f = 0;
    // La variable g représente le coût total d'uniter pour le deplacement,
    // soit le nombre total de tuile à parcourire pour ce rendre à la destiantion de n'importequel direction
    // à partire du point de départ.
    public float g = 0;
    // La varibale h représente le coût heuristique estimé pour differente direction.  
    public float h = 0;

    // Update is called once per frame
    void Update()
    {
        
        if (positionActuel)
        {
            GetComponent<Renderer>().material.color = Color.magenta;
        }
        else if (cible)
        {
            GetComponent<Renderer>().material.color = Color.cyan;
        }
        else if (estSelectionable)
        {
            GetComponent<Renderer>().material.color = Color.red;
        }
        else
        {
            RetourMaterielPrecedant();
            
        }
        
    }

    /// <summary>
    /// Remets à zéro certaines valeurs des tuiles.
    /// </summary>
    public void RetourArriere()
    { 
        adjacenteList.Clear();  
        
        positionActuel = false;
        cible = false;
        estSelectionable = false;
             
        visiter = false;
        parent = null;
        distance = 0;
        f = g = h = 0;

    }
    /// <summary>
    /// La fonction TrouverVoisin sert à trouver les tuiles voisines sur lesquelles
    /// le joueur peut se déplacer.
    /// </summary>
    public void TrouverVoisin(Tuile cibleTuile) 
    {
        RetourArriere();
        RegarderTuille(Vector3.forward, cibleTuile);
        RegarderTuille(-Vector3.forward, cibleTuile);
        RegarderTuille(Vector3.right, cibleTuile);
        RegarderTuille(-Vector3.right, cibleTuile);
    }

    /// <summary>
    /// Fonction permettant la vérification des tuiles.
    /// La fonction vérifie d'abord si les tuiles son des tuiles et si elles sont marchable, 
    /// si elles sont martchable la fonction les ajoute a la liste adjacenteList.
    /// </summary>
    /// <param name="direction">direction dans la quel le joueur peut se déplacer</param>
    public void RegarderTuille(Vector3 direction, Tuile cibleTuile)
    {
        Vector3 halfExtents = new Vector3(0.25f, 0.25f, 0.25f);
        Collider[] coliders = Physics.OverlapBox(transform.position + direction, halfExtents);
        
        foreach (Collider c in coliders)
        {
            Tuile tuile = c.gameObject.GetComponent<Tuile>();
            if (tuile != null && tuile.estMarchable)
            {                
                RaycastHit hit;
                if (!Physics.Raycast(tuile.transform.position, Vector3.up, out hit, 1) || (tuile == cibleTuile))
                {                
                    adjacenteList.Add(tuile);
                }               
            }
        }
    }

    /// <summary>
    /// Remets le matériel de base aux tuiles.
    /// </summary>
    public void RetourMaterielPrecedant()
    {
        if (Tags.CompareTags("Terrain", gameObject))
        {
            GetComponent<Renderer>().material = Resources.Load<Material>("Materials/Terrain");
        }
        if (Tags.CompareTags("Foret", gameObject))
        {
            GetComponent<Renderer>().material = Resources.Load<Material>("Materials/Foret");
        }
        if (Tags.CompareTags("Eau", gameObject))
        {
            GetComponent<Renderer>().material = Resources.Load<Material>("Materials/Eau");
            estMarchable = false;
        }
        if (Tags.CompareTags("Vierge", gameObject))
        {
            GetComponent<Renderer>().material = Resources.Load<Material>("Materials/Vierge");
        }
    }
    
}
