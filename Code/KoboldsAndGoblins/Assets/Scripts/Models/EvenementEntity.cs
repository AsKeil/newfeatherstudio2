﻿

namespace Models
{
    /// <summary>
    /// DEF-5-6
    /// Action positive ou négative qui survient dans la phase de gestion du village ou lors d'une expédition.
    /// Un événement peut parfois déclencher une phase de combat.
    /// </summary>
    public class EvenementEntity
    {
        public string nom;
        public string description;
        public string texteAgrement;
        public int chrono;
        public int groupeEvenement; // N'est pas implémenté. // 0 ou null quand l'événement n'a pas de combattants.

        /// <summary>
        /// Crée un événement qui doit survenir à un moment précis.
        /// </summary>
        /// <param name="nom"> Le nom de l'événement. </param>
        /// <param name="description"> La description de l'événement à des fins utilitaires. </param>
        /// <param name="texteAgrement"> Le texte d'agrément qui accompagne la description. </param>
        /// <param name="chrono"> Le nombre de tour avant que l'événement ne survienne. </param>
        public EvenementEntity(string nom, string description, string texteAgrement, int chrono)
        {
            CreerEvenement(nom, description, texteAgrement, chrono);
        }

        /// <summary>
        /// Crée un événement qui doit survenir immédiatement.
        /// </summary>
        /// <param name="nom"> Le nom de l'événement. </param>
        /// <param name="description"> La description de l'événement à des fins utilitaires. </param>
        /// <param name="texteAgrement"> Le texte d'agrément qui accompagne la description. </param>
        public EvenementEntity(string nom, string description, string texteAgrement)
        {
            CreerEvenement(nom, description, texteAgrement,0);
        }

        /// <summary>
        /// Crée un événement.
        /// </summary>
        /// <param name="nom"> Le nom de l'événement. </param>
        /// <param name="description"> La description de l'événement à des fins utilitaires. </param>
        /// <param name="texteAgrement"> Le texte d'agrément qui accompagne la description. </param>
        /// <param name="chrono"> Le nombre de tour avant que l'événement ne survienne. </param>
        private void CreerEvenement(string nom, string description, string texteAgrement, int chrono)
        {
            this.nom = nom;
            this.description = description;
            this.texteAgrement = texteAgrement;
            this.chrono = chrono;
            this.groupeEvenement = 0;
        }
    }
}
