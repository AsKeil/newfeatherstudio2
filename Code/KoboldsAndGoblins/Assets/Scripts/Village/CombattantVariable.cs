﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Variable qui spécifie quel combattant il est.
/// Associer au prefab Combattant.
/// </summary>
public class CombattantVariable : MonoBehaviour
{
    [HideInInspector]
    public Models.CombattantsEntity combattant;
}
