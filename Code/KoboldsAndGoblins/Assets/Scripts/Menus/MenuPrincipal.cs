﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Classe qui s'assure de populer l'encyclopédie au démarrage du jeu.
/// </summary>
public class MenuPrincipal : MonoBehaviour
{
    private void Start()
    {
        // S'assure que l'encyclopédie est populer au démarrage du jeu.
        new DataBank.PopulerEncyclopedie();
    }

    /// <summary>
    /// Quitte le jeu.
    /// </summary>
    public void Quitter()
    {
        Application.Quit();
    }
}
