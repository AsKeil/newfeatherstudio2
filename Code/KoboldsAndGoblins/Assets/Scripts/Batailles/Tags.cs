﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tags
{
    /// <summary>
    /// Fonction permettant de comparer les tags des GameObject pour s'assurer qu'un objet
    /// qui a un double tag.
    /// Un double tag est un tag séparer par une virgule ex: Tuile,Foret
    /// </summary>
    /// <param name="tag">Tag rechercher dans le GameObject</param>
    /// <param name="obj">Le GameObject que l'on souhaite savoir s’il a le tag rechercher</param>
    /// <returns></returns>
    public static bool CompareTags(string tag, GameObject obj)
    {
        string[] tabTags = obj.tag.Split(',');
        foreach (string str in tabTags)
        {
            if (tag == str) 
                return true;
        }
        return false;
    }
}
