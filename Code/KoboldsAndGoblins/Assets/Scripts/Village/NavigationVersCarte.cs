﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationVersCarte : MonoBehaviour
{

    /// <summary>
    /// REQ-8-8 Accéder à la carte du monde.
    /// Charge la scène de carte du monde.
    /// </summary>
    public void LoadCarteDuMonde()
    {
        SceneManager.LoadScene("MenuCarteDuMonde");
    }

}
