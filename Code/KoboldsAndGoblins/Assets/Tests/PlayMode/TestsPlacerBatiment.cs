﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestsPlacerBatiment
    {
        public GameObject testGrid, testGameManager, testHighlight, testCamera;

        /// <summary>
        /// Permet de créer un environnement semi-indépendant du jeu en soit
        /// pour pouvoir tester le script PlacerBatiment.
        /// </summary>
        void CreerEnvironnementTest(int nbBatimentVoulu)
        {
            testCamera = new GameObject();
            testCamera.AddComponent<Camera>();
            testCamera.transform.tag = "MainCamera";

            float grandeur = 2f;
            testGrid = new GameObject();
            testGrid.AddComponent<Grid>();
            testGrid.GetComponent<Grid>().SetGrandeur(grandeur);

            ApplicationData.NouvellePartie(ApplicationData.ListeRacesE[0],"Test");
            ApplicationData.listeBatimentsConstruits = new List<Models.BatimentEntity>();
            ApplicationData.listeBatimentsConstruits.Clear();

            Models.BatimentEntity testBatiment = ApplicationData.ListeBatimentsE[0];
            for (int i = 0; i < nbBatimentVoulu; i++)
            {
                ApplicationData.listeBatimentsConstruits.Add(new Models.BatimentEntity(testBatiment, 0, 10, i*2, i*2));
            }

            testHighlight = new GameObject();

            testGameManager = new GameObject();
            testGameManager.AddComponent<PlacerBatiment>();
            testGameManager.GetComponent<PlacerBatiment>().Highlight = testHighlight;
        }

        /// <summary>
        /// Test pour vérifier si lorsque que l'on crée aucun bâtiment, aucun bâtiment n'est chargé.
        /// </summary>
        [UnityTest]
        public IEnumerator TestsStartSansBatimentConstruit()
        {
            CreerEnvironnementTest(0);

            Assert.AreEqual(ApplicationData.listeBatimentsConstruits.Count,0);
            yield return null;
        }

        /// <summary>
        /// Test pour vérifier si lorsque que l'on crée un bâtiment, un seul bâtiment est chargé.
        /// </summary>
        [UnityTest]
        public IEnumerator TestsStartAvecUnBatimentConstruit()
        {
            CreerEnvironnementTest(1);

            Assert.AreEqual(ApplicationData.listeBatimentsConstruits.Count, 1);
            yield return null;
        }

        /// <summary>
        /// Test pour vérifier si lorsque que l'on crée cinq bâtiments, cinq bâtiments sont chargés.
        /// </summary>
        [UnityTest]
        public IEnumerator TestsStartAvecCinqBatimentConstruit()
        {
            CreerEnvironnementTest(5);

            Assert.AreEqual(ApplicationData.listeBatimentsConstruits.Count, 5);
            foreach (Models.BatimentEntity batiment1 in ApplicationData.listeBatimentsConstruits)
            {
                foreach (Models.BatimentEntity batiment2 in ApplicationData.listeBatimentsConstruits)
                {
                    if (!(batiment1.Equals(batiment2)))
                    {
                        Assert.AreNotEqual(batiment1.positionX, batiment2.positionX);
                        Assert.AreNotEqual(batiment1.positionY, batiment2.positionY);
                    }
                }
            }
            yield return null;
        }

        /// <summary>
        /// Test pour vérifier si le mode construction est bien actif lorsque l'on utilise la méthode
        /// ActiverContruction avec comme valeur true
        /// </summary>
        [UnityTest]
        public IEnumerator TestsActiverConstructionTrue()
        {
            CreerEnvironnementTest(1);
            testGameManager.GetComponent<PlacerBatiment>().ActiverConstruction(true, ApplicationData.listeBatimentsConstruits[0]);
            Assert.IsTrue(testGameManager.GetComponent<PlacerBatiment>().GetEstActif());
            Assert.AreEqual(testGameManager.GetComponent<PlacerBatiment>().batimentEntity, ApplicationData.listeBatimentsConstruits[0]);
            yield return null;
        }

        /// <summary>
        /// Test pour vérifier si le mode construction est bien actif lorsque l'on utilise la méthode
        /// ActiverContruction avec comme valeur false
        /// </summary>
        [UnityTest]
        public IEnumerator TestsActiverConstructionFalse()
        {
            CreerEnvironnementTest(1);
            testGameManager.GetComponent<PlacerBatiment>().ActiverConstruction(false, ApplicationData.listeBatimentsConstruits[0]);
            Assert.IsFalse(testGameManager.GetComponent<PlacerBatiment>().GetEstActif());
            Assert.AreEqual(testGameManager.GetComponent<PlacerBatiment>().batimentEntity, ApplicationData.listeBatimentsConstruits[0]);
            yield return null;
        }
        
        /// <summary>
        /// Test de non-régression lié à l'issue #18
        /// </summary>
        [UnityTest]
        public IEnumerator TestsSauvegarderBatiment()
        {
            CreerEnvironnementTest(2);
            ApplicationData.SauvegarderPartie();
            ApplicationData.ChargerPartie(ApplicationData.ListeSauvegardes[ApplicationData.ListeSauvegardes.Count-1]);
            Assert.AreNotEqual(ApplicationData.listeBatimentsConstruits[0].positionX, ApplicationData.listeBatimentsConstruits[1].positionX);
            Assert.AreNotEqual(ApplicationData.listeBatimentsConstruits[0].positionY, ApplicationData.listeBatimentsConstruits[1].positionY);
            ApplicationData.SupprimerSauvegarde(ApplicationData.ListeSauvegardes[ApplicationData.ListeSauvegardes.Count - 1]);
            yield return null;
        }

    }
}
