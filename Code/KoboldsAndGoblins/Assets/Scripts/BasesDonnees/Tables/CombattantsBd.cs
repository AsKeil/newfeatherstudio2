﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "Combatttants" dans la base de données "Partie"
    /// </summary>
    public class CombatantsBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champRacesEid = "RacesEid";
        private const string _champNom = "nom";
        private const string _champPointsDeVieMaximum = "pointsDevieMaximum";
        private const string _champPointsDeVieActuels = "pointsDevieActuels";
        private const string _champPointsDAttaque = "pointsDevieAttaque";
        private const string _champPointsDeDefense = "pointsDeDefense";
        private const string _champPorteeVision = "porteeVision";
        private const string _champPorteeDeplacement = "porteeDeplacement";
        private const string _champImage = "image";
        private const string _champEquipementsEquipementsEid1 = "equipementsEquipementsEid1";
        private const string _champEquipementsEquipementsEid2 = "equipementsEquipementsEid2";
        private const string _champEvenementsid = "Evenementsid";
        private const string _clePrimaire = "id";
        private RacesEBd _racesEBd;
        private EquipementsEBd _equipementsEBd;

        public string NomTable { get { return nom_table; } }
        public string ChampId { get { return _champId; } }

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdPartie"> Le nom de la BD de partie.</param>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD encyclopédie.</param>
        public CombatantsBd(string nomBdPartie, string nomBdEncyclopedie) : base(nomBdPartie, "Combattants")
        {
            _racesEBd = new RacesEBd(nomBdEncyclopedie);
            _equipementsEBd = new EquipementsEBd(nomBdEncyclopedie);
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champRacesEid + " INTEGER NOT NULL, "
                + _champNom + " TEXT NOT NULL, "
                + _champPointsDeVieMaximum + " INTEGER NOT NULL, "
                + _champPointsDeVieActuels + " INTEGER NOT NULL, "
                + _champPointsDAttaque + " INTEGER NOT NULL, "
                + _champPointsDeDefense + " INTEGER NOT NULL, "
                + _champPorteeVision + " INTEGER NOT NULL, "
                + _champPorteeDeplacement + " INTEGER NOT NULL, "
                + _champImage + " TEXT NOT NULL, "
                + _champEquipementsEquipementsEid1 + " INTEGER, "
                + _champEquipementsEquipementsEid2 + " INTEGER, "
                + _champEvenementsid + " INTEGER, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="combattant"> L'entité à ajouter dans la BD.</param>
        public void AddData(CombattantsEntity combattant)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champRacesEid + ", "
                + _champNom + ", "
                + _champPointsDeVieMaximum + ", "
                + _champPointsDeVieActuels + ", "
                + _champPointsDAttaque + ", "
                + _champPointsDeDefense + ", "
                + _champPorteeVision + ", "
                + _champPorteeDeplacement + ", "
                + _champImage + ", "
                // (Si combattant.arme est null)? (Si vrai: ", "):(Si faux: "")
                + ((combattant.arme != null)? (_champEquipementsEquipementsEid1 + ", "):(""))
                + ((combattant.armure != null) ? (_champEquipementsEquipementsEid2 + ", ") : (""))
                + _champEvenementsid
                + " ) "

                + "VALUES ( @race"
                + ", @nom"
                + ", @pointsDeVieMaximum"
                + ", @pointsDeVieActuels"
                + ", @pointsDAttaque"
                + ", @pointsDeDefense"
                + ", @porteeVision"
                + ", @porteeDeplacement"
                + ", @image"
                // (Si combattant.arme est null)? (Si vrai: ", id"):(Si faux: "")
                + ((combattant.arme != null) ? (", " + _equipementsEBd.GetIdFromEntity(combattant.arme)) : (""))
                + ((combattant.armure != null) ? (", " + _equipementsEBd.GetIdFromEntity(combattant.armure)) : (""))
                + ", @groupeEvenement"
                + " )";
            AddParameterWithValue(dbcmd, "@race", _racesEBd.GetIdFromEntity(combattant.race));
            AddParameterWithValue(dbcmd, "@nom", combattant.nom);
            AddParameterWithValue(dbcmd, "@pointsDeVieMaximum", combattant.pointsDeVieMaximum);
            AddParameterWithValue(dbcmd, "@pointsDeVieActuels", combattant.pointsDeVieActuels);
            AddParameterWithValue(dbcmd, "@pointsDAttaque", combattant.pointsDAttaque);
            AddParameterWithValue(dbcmd, "@pointsDeDefense", combattant.pointsDeDefense);
            AddParameterWithValue(dbcmd, "@porteeVision", combattant.porteeVision);
            AddParameterWithValue(dbcmd, "@porteeDeplacement", combattant.porteeDeplacement);
            AddParameterWithValue(dbcmd, "@image", combattant.image);
            AddParameterWithValue(dbcmd, "@groupeEvenement", combattant.groupeEvenement);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<CombattantsEntity> GetListeEntity()
        {
            System.Data.IDataReader reader = GetAllData();
            List<CombattantsEntity> liste = CombattantsEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<CombattantsEntity> CombattantsEntityParser(IDataReader reader)
        {
            List<CombattantsEntity> liste = new List<CombattantsEntity>();
            while (reader.Read())
            {
                CombattantsEntity combattant = new CombattantsEntity();
                combattant.race = _racesEBd.GetEntityFromId(reader[1].ToString());
                combattant.nom = reader[2].ToString();
                combattant.pointsDeVieMaximum = int.Parse(reader[3].ToString());
                combattant.pointsDeVieActuels = int.Parse(reader[4].ToString());
                combattant.pointsDAttaque = int.Parse(reader[5].ToString());
                combattant.pointsDeDefense = int.Parse(reader[6].ToString());
                combattant.porteeVision = int.Parse(reader[7].ToString());
                combattant.porteeDeplacement = int.Parse(reader[8].ToString());
                combattant.image = reader[9].ToString();
                if (reader[10] != System.DBNull.Value)
                    combattant.arme = _equipementsEBd.GetEntityFromId(reader[10].ToString());
                if (reader[11] != System.DBNull.Value)
                    combattant.armure = _equipementsEBd.GetEntityFromId(reader[11].ToString());
                combattant.groupeEvenement = int.Parse(reader[12].ToString());
                liste.Add(combattant);
            }
            return liste;
        }

        /// <summary>
        /// Ferme toutes les connexions.
        /// </summary>
        public new void Close()
        {
            _racesEBd.Close();
            _equipementsEBd.Close();
            bdConnexion.Close();
            bdConnexion.Dispose();
        }
    }
}
