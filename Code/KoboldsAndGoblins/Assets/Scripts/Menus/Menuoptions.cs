﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

/// <summary>
/// Classe qui gère le menu des options.
/// </summary>
public class Menuoptions : MonoBehaviour
{
    public AudioMixer audioMixer;

    public Dropdown resolutionDropdown;

    Resolution[] resolutions;


    /// <summary>
    /// Permet de rechercher les réolutions possible de l'écran et les place dans le dropdwon.
    /// </summary>
    void Start()
    {
        if (this.gameObject.name.Equals("OptionVideo"))
        {

            resolutions = Screen.resolutions;

            resolutionDropdown.ClearOptions();

            List<string> optionslist = new List<string>();


            int currentResolutionIndex = 0;
            for (int i = 0; i < resolutions.Length; i++)
            {
                string option = resolutions[i].width + " x " + resolutions[i].height;
                optionslist.Add(option);

                if (resolutions[i].width == Screen.currentResolution.width &&
                    resolutions[i].height == Screen.currentResolution.height)
                {
                    currentResolutionIndex = i;
                }

            }

            resolutionDropdown.AddOptions(optionslist);
            resolutionDropdown.value = currentResolutionIndex;
            resolutionDropdown.RefreshShownValue();

        }
    }

    /// <summary>
    /// Fixe la résolution avec celle de l'écran.
    /// </summary>
    /// <param name="resolutionIndex">L'index de la resolution.</param>
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    /// <summary>
    /// Fixe le volume du jeu.
    /// </summary>
    /// <param name="volume">Le volume du mixer audio.</param>
    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    /// <summary>
    /// Fixe la qualité graphique du jeu.
    /// </summary>
    /// <param name="qualityIndex">L'index de la qualité graphique.</param>
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    /// <summary>
    /// Fixe le mode mode plein écran du jeu.
    /// </summary>
    /// <param name="isFullscreen">Le boolean du fullscreen.</param>
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
}
