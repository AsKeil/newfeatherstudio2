﻿using Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DataBank;
using UnityEngine.SceneManagement;

/// <summary>
/// Classe fait la gestion de la partie dans l'interface.
/// </summary>
public class GestionPartie : MonoBehaviour
{
    int indexSauvegarde;

    public GameObject child;

    public Transform ParentUI;


    /// <summary>
    /// Permet d'ajouter les sauvegardes dans le UI.
    /// </summary>
    void Start()
    {
        int nbrSauvegardes = ApplicationData.ListeSauvegardes.Count;

        foreach (SauvegardeEntity sauvegarde in ApplicationData.ListeSauvegardes)
        {
            GameObject go = GameObject.Instantiate(child, ParentUI);
            go.transform.Find("NomSauvegardeText (TMP)").GetComponent<TextMeshProUGUI>().SetText(sauvegarde._nomVillage);
            go.transform.Find("DateSauvegardeText (TMP)").GetComponent<TextMeshProUGUI>().SetText(sauvegarde._dateHeure);
            go.transform.Find("ChargerButton").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { Chargerpartie(sauvegarde); });
            go.transform.Find("SupprimerButton").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { SupprimerSauvegarde(go, sauvegarde); });

        }

    }

    /// <summary>
    /// Permet de charger une partie.
    /// </summary>
    /// <param name="sauvegarde">La sauvegarde de la partie à charger</param>
    public void Chargerpartie(SauvegardeEntity sauvegarde)
    {
        ApplicationData.ChargerPartie(sauvegarde);
        SceneManager.LoadScene("SceneVillage");
        
    }

    /// <summary>
    /// Permet de supprimer une partie.
    /// </summary>
    /// <param name="ui">Le ui de la partie à supprimer</param>
    /// <param name="sauvegarde">La sauvegarde de la partie à supprimer</param>
    public void SupprimerSauvegarde(GameObject ui, SauvegardeEntity sauvegarde)
    {
        GameObject.Destroy(ui);
        ApplicationData.SupprimerSauvegarde(sauvegarde);
    }


}
