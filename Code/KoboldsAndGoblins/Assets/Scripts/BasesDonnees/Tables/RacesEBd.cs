﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Models;

namespace DataBank
{
    /// <summary>
    /// La spécificité d'une table de BD.
    /// Table "RacesE" dans la base de données "Encyclopedie".
    /// </summary>
    public class RacesEBd : SqliteHelper
    {
        private const string _champId = "id";
        private const string _champNom = "nom";
        private const string _clePrimaire = "id";

        public string NomTable {get{return nom_table; }}
        public string ChampId {get{ return _champId;}}

        /// <summary>
        /// Crée la table.
        /// </summary>
        /// <param name="nomBdEncyclopedie" > Le nom de la BD encyclopédie.</param>
        public RacesEBd(string nomBdEncyclopedie) : base(nomBdEncyclopedie, "RacesE")
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "CREATE TABLE IF NOT EXISTS " + nom_table + " ("
                + _champId + " INTEGER NOT NULL, "
                + _champNom + " TEXT NOT NULL, "
                + "PRIMARY KEY(" + _clePrimaire + "))";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Fait un ajout à la table.
        /// </summary>
        /// <param name="race"> L'entité à ajouter dans la BD.</param>
        public void AddData(RaceEntity race)
        {
            DbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "INSERT INTO " + nom_table
                + " ( "
                + _champNom
                + " ) "

                + "VALUES ( "
                + "@nomRace"
                + " )";
            AddParameterWithValue(dbcmd, "@nomRace", race.nomRace);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        /// <summary>
        /// Crée une entité pour chacune des entrées de la table.
        /// </summary>
        public List<RaceEntity> GetListeEntity()
        {
            IDataReader reader = GetAllData();
            List<RaceEntity> liste = RacesEntityParser(reader);
            reader.Dispose();
            return liste;
        }

        /// <summary>
        /// Retourne une entité à partir de son ID.
        /// </summary>
        /// <param name="id"> Le ID de l'entité cherchée.</param>
        /// <returns> L'entité cherchée.</returns>
        public RaceEntity GetEntityFromId(string id)
        {
            IDataReader reader = GetDataFromString(_champId,id);
            List<RaceEntity> liste = RacesEntityParser(reader);
            reader.Dispose();
            return liste[0];
        }

        /// <summary>
        /// Parse les données d'un reader pour retourner une liste d'entité.
        /// </summary>
        /// <param name="reader"> Le reader qu'il faut analyser. (parse) </param>
        /// <returns> Une liste d'entité.</returns>
        private List<RaceEntity> RacesEntityParser(IDataReader reader)
        {
            List<RaceEntity> liste = new List<RaceEntity>();
            while (reader.Read())
            {
                RaceEntity race = new RaceEntity(reader[1].ToString());
                liste.Add(race);
            }
            return liste;
        }

        /// <summary>
        /// Retourne le ID d'une race à partir de son nom.
        /// </summary>
        /// <param name="race"> L'entité dont on cherche le ID</param>
        /// <returns> Le ID de l'entitée.</returns>
        internal string GetIdFromEntity(RaceEntity race)
        {
            IDataReader reader = GetDataFromString(_champNom, race.nomRace);
            string id = reader[0].ToString();
            reader.Dispose();
            return id;
        }
    }
}
