﻿using System.Collections.Generic;
using UnityEngine;

public class PlacerBatiment : MonoBehaviour
{
    // Grille utilisée pour placer les bâtiments.
    private Grid grid;
    // Valeurs pour s'assurer que le script est actif et que l'endroit visé est vide.
    private bool estActif, estVide;
    // La position sur la grille la plus proche de l'endroit cliqué par le joueur.
    private Vector3 finalPosition;
    // La position de l'endroit cliqué par le joueur.
    private RaycastHit hitInfo;
    // Liste de tous les bâtiments de la scène.
    public List<GameObject> listeBatiment;
    // Objet qui affiche l'endroit que le bâtiment va être construit.
    public GameObject Highlight;
    // Materiel utilisé pour montrer si le bâtiment peut être bâti ou non.
    public Material GoodMat, BadMat;
    // Batiment en construction
    public Models.BatimentEntity batimentEntity;

    /// <summary>
    /// REQ-8-2 Afficher les bâtiments construits
    /// Permet de charger et de configurer certaines options au chargement de la scène.
    /// Crée les objets bâtiments qui font partie de la liste des bâtiments construits d'ApplicationData.
    /// </summary>
    void Start()
    {
        grid = FindObjectOfType<Grid>();
        estActif = false ;
        estVide = true ;
        Highlight.transform.position = new Vector3(0,0,0);
        Highlight.SetActive(estActif);
        listeBatiment = new List<GameObject>();
        if (ApplicationData.listeBatimentsConstruits != null)
        {
            foreach (Models.BatimentEntity batiment in ApplicationData.listeBatimentsConstruits)
            {
                finalPosition = new Vector3(batiment.positionX, 1, batiment.positionY);
                PlaceCubeNear();
            }
        }
    }

    /// <summary>
    /// REQ-8-3 Construire des bâtiments.
    /// Méthode qui s'assure que si le script est actif, il affiche la position
    /// possible du bâtiment et place le bâtiment lorsqu'il y a un clique-gauche
    /// de la souris.
    /// </summary>
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (estActif && Physics.Raycast(ray, out hitInfo))
        {
            finalPosition = grid.GetNearestPointOnGrid(hitInfo.point);

            foreach (GameObject Batiment in listeBatiment)
            {
                if (Batiment.transform.position == finalPosition)
                {
                    estVide = false;
                    Highlight.GetComponent<Renderer>().material = BadMat;
                    break;
                }
                else
                {
                    estVide = true;
                    Highlight.GetComponent<Renderer>().material = GoodMat;
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                if(estVide)
                {
                    PlaceCubeNear();
                    estActif = false;
                }
            }
        }
    }

    /// <summary>
    /// S'assure de placer l'objet Highlight à la bonne position et de l'activer ou le désactiver.
    /// </summary>
    private void LateUpdate()
    {
        Highlight.SetActive(estActif);
        Highlight.transform.position = finalPosition;
    }

    /// <summary>
    /// REQ-8-3 Construire des bâtiments.
    /// Place un bâtiment.
    /// </summary>
    private void PlaceCubeNear()
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = finalPosition;
        cube.tag = "Batiment";
        listeBatiment.Add(cube);
        if (estActif)
        {
            ApplicationData.listeBatimentsConstruits.Add(new Models.BatimentEntity(batimentEntity, 0, 10, (int)finalPosition.x, (int)finalPosition.z));
        }
    }
    /// <summary>
    /// Retourne la valeur de EstActif.
    /// </summary>
    /// <returns></returns>
    public bool GetEstActif() { return estActif; }

    /// <summary>
    /// Permet de modifier si la construction est active et le bâtiment à construire.
    /// </summary>
    /// <param name="estActif">Si la construction est activée ou non.</param>
    /// <param name="batimentEntity">Le bâtiment à construire.</param>
    public void ActiverConstruction(bool estActif, Models.BatimentEntity batimentEntity) 
    { 
        this.estActif = estActif;
        this.batimentEntity = batimentEntity;
    }

}
