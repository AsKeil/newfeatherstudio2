using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class gerant les tours des equipes et des combatants
/// </summary>
public class GestionaireTour : MonoBehaviour
{
    // Dictionnaire contenant tous les combattants de la grille.
    public static Dictionary<string, List<ActionCombatants>> combatants = new Dictionary<string, List<ActionCombatants>>();

    // File contenant l'équipe active.
    static Queue<string> tourCle = new Queue<string>();

    //File contenant l'ordre de jeux des combattants.
    static Queue<ActionCombatants> tourEquipe = new Queue<ActionCombatants>();
    
    //Bool qui signifie que l'equipe alie a gagner.
    public bool allieGagne;
    public bool ennemiGagne;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if( tourEquipe.Count == 0)
        {       
            InitTourEquipeQueue();
        }      
        if(allieGagne || ennemiGagne)
        {
            FinPartie();
            AffichageEcrantFin();
            enabled = false;
        }
        SuprimerCombatant();
    }



    /// <summary>
    /// Initialise la file d'une équipe selon l'ordre de la file.
    /// </summary>
    static void InitTourEquipeQueue()
    {
        List<ActionCombatants> listEquipe = combatants[tourCle.Peek()];
        foreach(ActionCombatants combatant in listEquipe)
        {
            tourEquipe.Enqueue(combatant);
        }
        CommencementTour();
    }

    /// <summary>
    /// Fonction débutant le tour d'un combattant à la fois.
    /// </summary>
    public static void CommencementTour()
    {
        if(tourEquipe.Count>0)
        {
            tourEquipe.Peek().GetComponent<ActionCombatants>().DebutCombatantTour();
        }
    }

    /// <summary>
    /// Si tous les combattants d'une équipe ont bougé, le tour est fini.
    /// </summary>
    public static void FinTour()
    {
        ActionCombatants combatant = tourEquipe.Dequeue();
        combatant.GetComponent<ActionCombatants>().FinCombatantTour();
        if (tourEquipe.Count > 0)
        {
            CommencementTour();
        }
        else
        {
            string equipe = tourCle.Dequeue();
            tourCle.Enqueue(equipe);
            InitTourEquipeQueue();
        }
    }

    /// <summary>
    /// Fonction ajoutant les combattants dans le dictionnaire selon leur équipe.
    /// </summary>
    /// <param name="combatant"> combatant d'un équipe</param>
    public static void AjouterCombatant(ActionCombatants combatant)
    {
        List<ActionCombatants> list;
        if (!combatants.ContainsKey(combatant.tag))
        {
            //si l'équipe du combattant n'est pas dans le dictionnaire elle est ajouté ici
            list = new List<ActionCombatants>();
            combatants[combatant.tag] = list;
            if (!tourCle.Contains(combatant.tag))
            {
                tourCle.Enqueue(combatant.tag);
            }
        }
        else
        {
            list = combatants[combatant.tag];
        }
        list.Add(combatant);
    }
    /// <summary>
    /// Supprime le combattant mort de tous les listes et endroits du champ de bataille.
    /// </summary>
    public void SuprimerCombatant()
    {
        bool listVide = false;
        List<ActionCombatants> list;
        foreach (KeyValuePair<string, List<ActionCombatants>> listCombatant in combatants)
        {
            list = combatants[listCombatant.Key];
            if (list != null)
            {
                foreach (ActionCombatants combatant in list)
                {
                    if (combatant.estMort)
                    {                       
                        combatant.gameObject.SetActive(false);
                        list.Remove(combatant);                        
                       
                    }
                    if (list.Count == 0)
                    {
                        listVide = true;
                        break;
                    }
                }
            }
            if (listVide)
            {
                DefinieEquipeGagnante(listCombatant);
                listVide = false;
            }           
        }
    }
    /// <summary>
    /// Fonction permettant de défenir quelle équipe qui a gagner selon l'équipe qui n'a pus de combattants
    /// </summary>
    /// <param name="listCombatant"> La liste de combattant vide</param>
    public void DefinieEquipeGagnante(KeyValuePair<string, List<ActionCombatants>> listCombatant)
    {
        if (listCombatant.Key == "Ennemi")
        {
            allieGagne = true;            
        }
        if (listCombatant.Key == "Allie")
        {
            ennemiGagne = true;
        }
    }
    private void FinPartie()
    {
        List<ActionCombatants> list;
        list = new List<ActionCombatants>();
        foreach (KeyValuePair<string, List<ActionCombatants>> k in combatants)
        {
            list = combatants[k.Key];
            if (list != null)
            {
                foreach (ActionCombatants combatant in list)
                {
                    combatant.gameObject.SetActive(false);
                }
            }
        }
        tourEquipe.Clear();
        tourCle.Clear();
        combatants.Clear();
    }

    /// <summary>
    /// affiche un écran de victoire temporaire pour signaler que le combat est terminé.
    /// </summary>
    public void AffichageEcrantFin()
    {
        GameObject.Find("Panel").GetComponent<Image>().enabled=true;
        GameObject.Find("BoutonRetourVillage").GetComponent<Image>().enabled = true;
        GameObject.Find("BoutonRetourVillage").GetComponent<Button>().enabled = true;
        GameObject.Find("BoutonRetourVillage").GetComponentInChildren<Text>().enabled = true;
        if (allieGagne)
        {
            GameObject.Find("PanelTexte").GetComponent<Text>().text = "Félicitation Vous avez gagné!!!";
        }
        else if (ennemiGagne)
        {
            GameObject.Find("PanelTexte").GetComponent<Text>().text = "Vous avez perdu!!!";
        }
        GameObject.Find("PanelTexte").GetComponent<Text>().enabled = true;      
    }
}
